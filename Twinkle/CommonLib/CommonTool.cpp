#include "CommonTool.h"
#include "MyLog.h"
#include<TlHelp32.h> 
#include <Shlwapi.h>
#pragma comment(lib,"shlwapi.lib")
#pragma warning(disable:4996) 

/************************************************************************
* 通过游戏类名查找句柄列表
************************************************************************/
std::vector<HWND> FindHwndByClass(TCHAR *szGame)
{
	std::vector<HWND> RtnHwnd;
	HWND hDesktopHwnd = 0;	//顶级窗口的句柄（桌面）
	HWND hOneHwnd = 0;		//第一个窗口的句柄
	HWND hWnd1 = 0,hWnd2 = 0;
	BOOL hWndok = FALSE;	//遍历完成标志
	hDesktopHwnd = ::GetDesktopWindow();				//获取顶级窗口的句柄
	hOneHwnd = ::GetWindow(hDesktopHwnd, GW_CHILD);		//获取桌面下第一个窗口的句柄
	hWnd1 = hOneHwnd;
	do 
	{
		hWnd2 = ::GetWindow(hWnd1, GW_HWNDNEXT);		//获取下一个同级窗口的句柄
		hWnd1 = hWnd2;
		CString strClassname;
		TCHAR szClass[MAX_PATH];
		::GetClassName(hWnd2, szClass, MAX_PATH);	//获取窗口的类名
		strClassname.Format(_T("%s"), szClass);
		if (strClassname.Find(szGame) == 0){
			WRITE_LOG("找到游戏窗口，句柄 -> 0x%08X",hWnd2);
			RtnHwnd.push_back(hWnd2);
		}
		else if (strClassname.Find(_T("Progman")) == 0){
			//ERROR_MSG("遍历完成，退出循环!");
			hWndok = TRUE;
		}
	} while (hWndok == FALSE);	
	return RtnHwnd;
}

/************************************************************************
* 将单字节char*转化为宽字节wchar_t*  
************************************************************************/
wchar_t* AnsiToUnicode( const char* szStr )  
{  
	int nLen = MultiByteToWideChar( CP_ACP, MB_PRECOMPOSED, szStr, -1, NULL, 0 );  
	if (nLen == 0)  
	{  
		return NULL;  
	}  
	wchar_t* pResult = new wchar_t[nLen];  
	MultiByteToWideChar( CP_ACP, MB_PRECOMPOSED, szStr, -1, pResult, nLen );  
	return pResult;  
}  

/************************************************************************
* 将宽字节wchar_t*转化为单字节char*  
************************************************************************/
char* UnicodeToAnsi( const wchar_t* szStr )  
{  
	int nLen = WideCharToMultiByte( CP_ACP, 0, szStr, -1, NULL, 0, NULL, NULL );  
	if (nLen == 0)  
	{  
		return NULL;  
	}  
	char* pResult = new char[nLen];  
	WideCharToMultiByte( CP_ACP, 0, szStr, -1, pResult, nLen, NULL, NULL );  
	return pResult;  
}  

/************************************************************************
*  模式匹配字符串
************************************************************************/
bool IsBeginSpecChar(char ch)
{
	return ch == '(' || ch == '[' || ch == '{';
}
bool IsEndSpecChar(char ch)
{
	return ch == ')' ||  ch == ']'  || ch == '}';
}
void PatterStr2(const char* szStr, std::vector<CString>& strStrs)
{
	char *szTmp = NULL;
	int iLen = strlen(szStr);
	szTmp = new char[iLen];
	memcpy(szTmp, szStr, iLen);
	//
	int iBegin = -1;
	int iEnd = -1;
	for(int i = 0; i < iLen; i ++){
		if (IsBeginSpecChar(szTmp[i]))
			iBegin = i;
		if(IsEndSpecChar(szTmp[i]))
			iEnd = i;
		if(iBegin > 0 && iEnd >0){
			char OldChar = szTmp[iEnd];
			szTmp[iEnd] = '\0';
			//
			wchar_t *pResult = AnsiToUnicode( szTmp + iBegin + 1);
			strStrs.push_back(pResult);
			delete [] pResult;
			//
			szTmp[iEnd] = OldChar;
			iBegin = -1;
			iEnd = -1;
		}
	}
	delete [] szTmp;
}
void PatterStr(const char* szStr, std::vector<CString>& strStrs) 
{
	__try{
		PatterStr2(szStr, strStrs);
	}
	__except(1){
		WRITE_LOG("字符串模式匹配挂掉了:%s", szStr);
	}
}


/************************************************************************
*  根据进程id，获取进程中模块信息
************************************************************************/
 BOOL GetModsByProcessID(DWORD dwID, std::vector<CString>& strModules, std::vector<CString>& strModulePath)   
{  
	HANDLE hModuleSnap = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwID);  
	if(hModuleSnap == INVALID_HANDLE_VALUE){   
		WRITE_LOG("查找模块时候,挂掉了！");
		return FALSE;
	}  
	MODULEENTRY32 module32;  
	module32.dwSize = sizeof(module32);  
	BOOL bResult = Module32First(hModuleSnap, &module32);  
	while(bResult){
		//ERROR_MSG("模块:%s",UnicodeToAnsi(module32.szModule));
		strModules.push_back(module32.szModule);
		strModulePath.push_back(module32.szExePath);
		bResult = Module32Next(hModuleSnap, &module32);  
	}  
	CloseHandle(hModuleSnap);  
	return FALSE;
}  

 /************************************************************************
 *  数据转换
 ************************************************************************/
int ToInt(CString strInt)
{
	return _ttoi(strInt);
}

int ToInt(const char* szInt)
{
	return atoi(szInt);
}

 /************************************************************************
 *  内存扫描
 ************************************************************************/
std::vector<DWORD> ScanAddress(char *szMarkCode)  
{  
	std::vector<DWORD> Adresses;
	//起始地址  
	const DWORD beginAddr = 0x00400000;  
	//结束地址  
	//const DWORD endAddr = 0x7FFFFFFF;    //理论上限
	const DWORD endAddr = 0x10000000;     //看了下实际不需要搜索全部的进程空间，这样速度快点
	//每次读取游戏内存数目的大小  
	DWORD pageSize;  

	////////////////////////处理特征码/////////////////////  
	//特征码长度不能为单数  
	if (strlen(szMarkCode) % 2 != 0) return Adresses;  
	//特征码长度  
	int iLen = strlen(szMarkCode) / 2;  
	//将特征码转换成byte型  
	BYTE *byCode = new BYTE[iLen];  
	for (int i = 0; i < iLen; i++){  
		char c[] = {szMarkCode[i*2],  szMarkCode[i*2+1], '\0'};  
		byCode[i] = (BYTE)::strtol(c, NULL, 16);  
	}  
	//忽略掉本身
	MEMORY_BASIC_INFORMATION MemInfo;
	/////////////////////////查找特征码/////////////////////  
	DWORD tmpAddr = beginAddr;  
	while (tmpAddr <= endAddr - iLen){  
		::VirtualQuery((LPCVOID)tmpAddr, &MemInfo, sizeof(MemInfo));
		pageSize = MemInfo.RegionSize;
		DWORD dwProtect = MemInfo.AllocationProtect;
		if(MemInfo.State == MEM_COMMIT && MemInfo.Protect==PAGE_READWRITE){  
			for (int i = 0; i < (int)pageSize; i++){  
				for (int j = 0; j < iLen; j++){  
					if (byCode[j] != ((BYTE*)tmpAddr)[i + j])break;  
					if (j == iLen - 1){  
						if((DWORD)byCode != tmpAddr +i)
							Adresses.push_back(tmpAddr +i);
					}  
				}  
			}  	
		}
		tmpAddr += pageSize;  
	}
	delete [] byCode;
	return Adresses;
}  

 /************************************************************************
 *  转成汉子数字, 30个够用了
 ************************************************************************/
CString ToHanzi(DWORD dwNum)
{
	CString strTab [31] = { 
		_T("零"),
		_T("一"), _T("二"),  _T("三"), _T("四"),   _T("五"),  
		_T("六"), _T("七"),  _T("八"), _T("九"),   _T("十"), 
		_T("十一"), _T("十二"),  _T("十三"), _T("十四"),   _T("十五"), 
		_T("十六"), _T("十七"),  _T("十八"), _T("十九"),   _T("二十"), 
		_T("二十一"), _T("二十二"),  _T("二十三"), _T("二十四"),   _T("二十五"), 
		_T("二十六"), _T("二十七"),  _T("二十八"), _T("二十九"),   _T("三十")
	};
	return strTab[dwNum];
}

 /************************************************************************
 *  地址转换
 ************************************************************************/
DWORD AddrToInt(DWORD dwAddr)
{
	return *(DWORD*)dwAddr;
}
BYTE AddrToBYTE(DWORD dwAddr)
{
		return *(BYTE*)dwAddr;
}


 /************************************************************************
 *  获取当前模块路径
 ************************************************************************/
CString GetModPathName(HMODULE hModule)
{
	CString strRtn;
	DWORD len = GetModuleFileName(hModule, strRtn.GetBufferSetLength(MAX_PATH + 2), MAX_PATH);
	return  strRtn;
}
 /************************************************************************
 *  获取当前模块目录
 ************************************************************************/
CString GetModDir(CString strFullPath)
{
	PathRemoveFileSpec(strFullPath.GetBuffer());
	return strFullPath;
}

 /************************************************************************
 *  获取当前模块名字
 ************************************************************************/
CString GetModFileName(CString strFullPath)
{
	PathStripPath(strFullPath.GetBuffer());
	return strFullPath;
}


 /************************************************************************
 *  获取当前模块日志名字
 ************************************************************************/
CString GetModLogName(CString strPathName)
{
	CString strModDir = GetModDir(strPathName);
	CString strFileName = GetModFileName(strPathName);
	int i = strFileName.ReverseFind(_T('.'));
	CString strExatName = strFileName.Left(i);
	//
	CString strLogFileName;
	strLogFileName += strModDir;
	strLogFileName += _T("\\");
	strLogFileName += strExatName;
	strLogFileName += _T(".log");
	return  strLogFileName;
}

 /************************************************************************
 *  查询字符串信息
 ************************************************************************/
bool strMember(const char* szType, char* szSpecTypes)
{
	char szTmp[MAX_PATH];
	strcpy(szTmp, szSpecTypes);
	char *p;
	p = strtok(szTmp, "#");
	 while(p){
		 if(strcmp(szType, p) == 0){
			 return true;
		 }
		 p = strtok(NULL, "#");     
	 }
	 return false;
}
