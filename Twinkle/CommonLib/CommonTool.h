#include <afx.h>
#include <Windows.h>
#include <vector>
#include <string.h>
#ifndef _H_COMMON_TOOL_H_
#define _H_COMMON_TOOL_H_
std::vector<HWND> FindHwndByClass(TCHAR *szGame);
wchar_t* AnsiToUnicode( const char* szStr );
char* UnicodeToAnsi( const wchar_t* szStr );
void PatterStr(const char* szStr, std::vector<CString>& strStrs);
BOOL GetModsByProcessID(DWORD dwID, std::vector<CString>& strModules, std::vector<CString>& strModulePath);
int ToInt(CString strInt);
int ToInt(const char* szInt);
CString ToHanzi(DWORD dwNum);
std::vector<DWORD> ScanAddress(char *markCode);
DWORD AddrToInt(DWORD dwAddr);
BYTE AddrToBYTE(DWORD dwAddr);
CString GetModPathName(HMODULE hModule);
CString GetModDir(CString strFullPath);
CString GetModFileName(CString strFullPath);
CString GetModLogName(CString strModName);
bool strMember(const char* szType, char* szSpecTypes);
#endif