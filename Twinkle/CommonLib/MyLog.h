#include <afx.h>
#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifndef _H_CMY_LOG_H_
#define _H_CMY_LOG_H_
class CMyLog;
#define  DEBUG_LOG
#ifdef DEBUG_LOG
	#define	INIT_MY_LOG(szFileName)	do{CMyLog::Instantialize(szFileName);}while(0);
	#define	DESTORY_MY_LOG() do{	CMyLog::Destory();}while(0); 
	#define  WRITE_LOG(szFormat, ...)  do {\
											if(CMyLog::pMyLog != NULL){\
												CMyLog::pMyLog->WriteLog(__FILE__, __LINE__, szFormat, __VA_ARGS__);\
											}\
										}while(0);

#else
	#define	INIT_MY_LOG(szFileName)
	#define	DESTORY_MY_LOG()
	#define  WRITE_LOG(szFormat, ...)
#endif
	
class CMyLog{
private:
	CMyLog();
	CMyLog(const CMyLog &);  
	CMyLog& operator = (const CMyLog &);  
public:
	static CMyLog* Instantialize(const char* szFileName);
	static void Destory();
	static CMyLog* pMyLog;  
	static CRITICAL_SECTION s_cs;
	static FILE *fp;
public:
	void WriteLog(const char *szFileName, int line, const char* szFormat, ...);
};

#endif