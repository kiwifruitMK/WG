
// LaunchDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "Launch.h"
#include "LaunchDlg.h"
#include "afxdialogex.h"
#include "Test.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif




// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CLaunchDlg 对话框




CLaunchDlg::CLaunchDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CLaunchDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CLaunchDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_list);
	DDX_Check(pDX, IDC_CHECK1, m_IsCheck);
}

BEGIN_MESSAGE_MAP(CLaunchDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON3, &CLaunchDlg::OnBnClickedButton3)
	ON_WM_DESTROY()
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_CHECK1, &CLaunchDlg::OnBnClickedCheck1)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CLaunchDlg 消息处理程序

BOOL CLaunchDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	LONG lStyle;
	lStyle = GetWindowLong(m_list.m_hWnd, GWL_STYLE);//获取当前窗口style
	lStyle &= ~LVS_TYPEMASK; //清除显示方式位
	lStyle |= LVS_REPORT; //设置style
	SetWindowLong(m_list.m_hWnd, GWL_STYLE, lStyle);//设置style

	DWORD dwStyle = m_list.GetExtendedStyle();
	dwStyle |= LVS_EX_FULLROWSELECT;//选中某行使整行高亮（只适用与report风格的listctrl）
	dwStyle |= LVS_EX_GRIDLINES;//网格线（只适用与report风格的listctrl）
	dwStyle |= LVS_EX_DOUBLEBUFFER; //双缓冲，防止闪烁
	//dwStyle |= LVS_EX_CHECKBOXES;//item前生成checkbox控件
	m_list.SetExtendedStyle(dwStyle); //设置扩展风格

	m_list.InsertColumn( 0,  _T("句柄ID"),  LVCFMT_LEFT,  80 );//插入列
	m_list.InsertColumn( 1, _T("角色名"),  LVCFMT_LEFT,  100);
	m_list.InsertColumn( 2, _T("是否隐藏"),  LVCFMT_LEFT,  70 );
	//
	SetTimer(TIMER_TYPE1, INIT_SEC, NULL);
	//
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CLaunchDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CLaunchDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CLaunchDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CLaunchDlg::OnBnClickedButton3()
{
	// TODO: 在此添加控件通知处理程序代码
	std::vector<HWND> hWnds;
	hWnds = FindHwndByClass(CLIENT_CLASS_NAME);
	std::vector<HWND>::iterator begin = hWnds.begin();
	std::vector<HWND>::iterator end = hWnds.end();
	for(; begin != end; begin ++){
		HWND hWnd = *begin;
		InstallHook(hWnd);
	}
}


void CLaunchDlg::OnDestroy()
{
	CDialogEx::OnDestroy();
	// TODO: 在此处添加消息处理程序代码
	DESTORY_MY_LOG();
	std::vector<HWND> hWnds;
	hWnds = FindHwndByClass(CLIENT_CLASS_NAME);
	std::vector<HWND>::iterator begin = hWnds.begin();
	std::vector<HWND>::iterator end = hWnds.end();
	for(; begin != end; begin ++){
		HWND hWnd = *begin;
		UninstallHook();
	}
}

void SyncTabList(CListCtrl& ClientList) 
{
	static std::map<HWND, int> mapRow;
	std::map<HWND, int>  mapTmp;   //专门用来删除的
	//
	std::vector<HWND> hWnds;
	hWnds = FindHwndByClass(CLIENT_CLASS_NAME);
	std::vector<HWND>::iterator begin = hWnds.begin();
	std::vector<HWND>::iterator end = hWnds.end();
	for(; begin != end; begin ++){
		CString strText;
		::GetWindowText(*begin, strText.GetBufferSetLength(MAX_PATH + 1), MAX_PATH);
		std::vector<CString > patterStr;
		char *szTmpText = strText.GetBuffer();
		PatterStr(szTmpText, patterStr); 
		//
		CString strHwnd;
		CString strIsShow;
		CString strName;
		//
		strHwnd.Format(_T("%d"), *begin);
		if (::IsWindowVisible(*begin)){
			strIsShow.Format(_T("否"));
		}else{
			strIsShow.Format(_T("是"));
		}
		if(patterStr.size() >= 3){
			strName = patterStr[2];
			if (strName.Find(_T("线")) >0){
				strName = _T("——");
			}else{
				strName = patterStr[2];
			}
		}else{
			strName = _T("——");
		}
		std::map<HWND, int>::iterator l_it;
		l_it=mapRow.find(*begin);
		if(l_it==mapRow.end()){
			int iRow = ClientList.GetItemCount();
			WRITE_LOG("新行号:%d", iRow);
			ClientList.InsertItem(iRow, strHwnd);
			ClientList.SetItemText(iRow, 1,  strName);
			ClientList.SetItemText(iRow, 2,  strIsShow);
		}else{
			int iRow = l_it->second;
			ClientList.SetItemText(iRow, 0,  strHwnd);
			ClientList.SetItemText(iRow, 1,  strName);
			ClientList.SetItemText(iRow, 2,  strIsShow);
			mapRow.erase(l_it);
		}
		//ERROR_MSG("进程ID:%d", dwProcessID);
	}
	std::map<HWND, int>::iterator mapRowBegin = mapRow.begin();
	std::map<HWND, int>::iterator mapRowEnd = mapRow.end();
	for(; mapRowBegin != mapRowEnd; mapRowBegin ++){
		ClientList.DeleteItem( mapRowBegin ->second);
	}
	//
	mapRow.clear();
	int iMaxRow = ClientList.GetItemCount();
	for(int i = 0; i< iMaxRow; i++){
		CString strText;
		ClientList.GetItemText(i, 0, strText.GetBufferSetLength(MAX_PATH + 1), MAX_PATH);
		//ERROR_MSG("重新算出ID:%d, 行号：%d",  ToInt(strText), i);
		mapRow.insert ( std::pair <HWND, int>((HWND)ToInt(strText), i) );
	}
	//ERROR_MSG("长度：%d", mapRow.size() );
}

void CLaunchDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	switch(nIDEvent)
	{
	case	 TIMER_TYPE1:
		SyncTabList(m_list);
		SetTimer(TIMER_TYPE1, THREE_SEC, NULL);
		break;
	default:
		//nothing
		;
	}

	CDialogEx::OnTimer(nIDEvent);
}


void CLaunchDlg::OnBnClickedCheck1()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	if(m_IsCheck){
		CString strPathName = GetModPathName(AfxGetInstanceHandle());
		CString strLogName =GetModLogName(strPathName);
		char* szLogFile = strLogName.GetBuffer();
		INIT_MY_LOG(szLogFile);
	}else{
		DESTORY_MY_LOG();
	}
	
	/*
	char szFile[] = "./Config/Launch.xml";
	tinyxml2::XMLDocument doc;  
	tinyxml2::XMLError eError = doc.LoadFile(szFile);
	tinyxml2::XMLElement *lanuch = doc.RootElement();
	tinyxml2::XMLElement * role_cfg = lanuch->FirstChildElement("role_cfg");
	tinyxml2::XMLElement  *add_point = role_cfg->FirstChildElement("add_point");
	while(add_point)
	{
		const char *polar = add_point->Attribute("polar");
		tinyxml2::XMLElement  *dex = add_point->FirstChildElement("dex");
		tinyxml2::XMLElement  *str = add_point->FirstChildElement("str");
		tinyxml2::XMLElement  *wiz = add_point->FirstChildElement("wiz");
		tinyxml2::XMLElement  *con = add_point->FirstChildElement("con");
		CString strLogout;
		strLogout.Format(
			_T("职业:%s, 敏捷:%s, 力量:%s,灵力:%s,体质:%s"),
			polar, 
			dex->GetText(),
			str->GetText(),
			wiz->GetText(),
			con->GetText());
		MessageBox(strLogout, "xml信息");
		con->SetText(1);
		add_point = add_point ->NextSiblingElement();
	}
	doc.SaveFile(szFile);
	*/
	
}

void CLaunchDlg::OnClose()
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	std::vector<HWND> hWnds;
	hWnds = FindHwndByClass(CLIENT_CLASS_NAME);
	std::vector<HWND>::iterator begin = hWnds.begin();
	std::vector<HWND>::iterator end = hWnds.end();
	for(; begin != end; begin ++){
		HWND hWnd = *begin;
		InstallHook(hWnd);
	}
	CDialogEx::OnClose();
}
