
// LaunchDlg.h : 头文件
//

#pragma once


// CLaunchDlg 对话框
class CLaunchDlg : public CDialogEx
{
// 构造
public:
	CLaunchDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_LAUNCH_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton3();
	afx_msg void OnDestroy();
	CListCtrl m_list;
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	BOOL m_IsCheck;
	afx_msg void OnBnClickedCheck1();
	afx_msg void OnClose();
};
