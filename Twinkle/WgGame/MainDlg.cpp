// MainDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "WgGame.h"
#include "MainDlg.h"
#include "afxdialogex.h"
#include "BaseSet.h"
#include "Skill.h"
#include "Account.h"
#include "Other.h"

#define TIMER_1_SEC  1   //秒循环
#define  TIMER_3_SEC 3   //三秒循环


CBaseSet  BaseSetDlg;
CSkill RoleSkill;
CAccount Account;
COther   Other;
extern CWgGameApp theApp;



// CMainDlg 对话框

IMPLEMENT_DYNAMIC(CMainDlg, CDialog)

CMainDlg::CMainDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMainDlg::IDD, pParent)
{

}

CMainDlg::~CMainDlg()
{
}

//这个后来加上的
void CMainDlg::OnFinalRelease()
{
	CDialog::OnFinalRelease();
}

void CMainDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB1, m_Tab);
	DDX_Control(pDX, IDC_LIST1, m_ListRecord);
}


BEGIN_MESSAGE_MAP(CMainDlg, CDialog)
	ON_WM_CLOSE()
	ON_WM_HOTKEY()
	ON_WM_DESTROY()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB1, &CMainDlg::OnTcnSelchangeTab1)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CMainDlg 消息处理程序


void CMainDlg::OnClose()
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	this ->ShowWindow(SW_HIDE);
}

BOOL CMainDlg::PreTranslateMessage(MSG* pMsg)
{
	return CDialog::PreTranslateMessage(pMsg);
}


BOOL CMainDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	//RegisterHotKey(GetSafeHwnd(), FAST_KEY_DEBUG, MOD_ALT, 'C');
	//
	CString strPathName = GetModPathName(theApp.m_hInstance);
	CString strLogName =GetModLogName(strPathName);
	char* szLogFile = strLogName.GetBuffer();
	INIT_MY_LOG(szLogFile);
	//
	m_Tab.InsertItem(0, _T("基本设置"));
	m_Tab.InsertItem(1, _T("技能设置"));
	m_Tab.InsertItem(2, _T("账号设置"));
	m_Tab.InsertItem(3, _T("其他设置"));
	BaseSetDlg.Create(IDD_BASE, GetDlgItem(IDC_TAB1));
	RoleSkill.Create(IDD_SKILL, GetDlgItem(IDC_TAB1));
	Account.Create(IDD_ACCOUNT, GetDlgItem(IDC_TAB1));
	Other.Create(IDD_OTHER, GetDlgItem(IDC_TAB1));
	CRect rs;
	m_Tab.GetClientRect(&rs);
	rs.top+=20;
	rs.bottom-=20;
	rs.left+=20;
	rs.right-=20;
	BaseSetDlg.MoveWindow(&rs);
	RoleSkill.MoveWindow(&rs);
	Account.MoveWindow(&rs);
	Other.MoveWindow(&rs);
	BaseSetDlg.ShowWindow(TRUE);
	RoleSkill.ShowWindow(FALSE);
	Account.ShowWindow(FALSE);
	Other.ShowWindow(FALSE);
	m_Tab.SetCurSel(0);
	// 设置定时器
	SetTimer(TIMER_1_SEC, 1000, NULL);
	SetTimer(TIMER_3_SEC, 3000, NULL);
	HookInit();
	//
	CString strDir = GetModDir(strPathName);
	CString strCfg;
	strCfg.Format(_T("%s\\%s\\%s"), strDir.GetBuffer(), CONFIG_DIR, WG_DLL_CONFIG);
	strcpy_s(szAppCfgPath, strCfg.GetBuffer());
	CString strLog;
	strLog.Format(_T("配置文件为:%s"), szAppCfgPath);
	LogRecord(strLog);
	LogRecord(_T("初始化完毕！"));
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}


void CMainDlg::OnHotKey(UINT nHotKeyId, UINT nKey1, UINT nKey2)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	CDialog::OnHotKey(nHotKeyId, nKey1, nKey2);
}


void CMainDlg::OnDestroy()
{
	CDialog::OnDestroy();
	DESTORY_MY_LOG();
	// TODO: 在此处添加消息处理程序代码
	//UnregisterHotKey(GetSafeHwnd(),  FAST_KEY_DEBUG);
}


void CMainDlg::OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult)
{
	// TODO: 在此添加控件通知处理程序代码
	int iSelect = 0;
	iSelect = m_Tab.GetCurSel();    //获得m_mtab 控件当前选中标签的索引
	BaseSetDlg.ShowWindow(FALSE);
	RoleSkill.ShowWindow(FALSE);
	Account.ShowWindow(FALSE);
	Other.ShowWindow(FALSE);
	switch(iSelect)
	{
	case 0:BaseSetDlg.ShowWindow(TRUE); break;
	case 1:RoleSkill.ShowWindow(TRUE); break;
	case 2:Account.ShowWindow(TRUE); break;
	case 3:Other.ShowWindow(TRUE); break;
	default:
		;
	}
	*pResult = 0;
}


void CMainDlg::LogRecord(CString strText)
{
	m_ListRecord.InsertString(LogNum(), strText);
	m_ListRecord.SendMessage(WM_VSCROLL, SB_PAGEDOWN , 0);
}


int CMainDlg::LogNum(void)
{

	 return	m_ListRecord.GetCount();
}

void CMainDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	switch(nIDEvent)
	{
	case TIMER_1_SEC:
		BaseSetDlg.Loop_1_Sec();
		break;
	case TIMER_3_SEC:
		BaseSetDlg.Loop_3_Sec();
		break;
	default:
		break;
	}
	CDialog::OnTimer(nIDEvent);
}


void CMainDlg::HookInit(void)
{
	BaseSetDlg.UpdateRoleInfo();
}
