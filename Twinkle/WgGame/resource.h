//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by WgGame.rc
//
#define IDP_SOCKETS_INIT_FAILED         101
#define IDD_BASE                        101
#define IDD_MAINDLG                     9000
#define IDC_TAB1                        9001
#define IDD_SKILL                       9001
#define IDD_ACCOUNT                     9002
#define IDD_OTHER                       9003
#define IDC_BUTTON3                     9005
#define IDC_BUTTON1                     9006
#define IDC_LIST1                       9008
#define IDC_CHECK1                      9009
#define IDC_ACCOUNT                     9010
#define IDC_EDIT2                       9011
#define IDC_PASSWD                      9011
#define IDC_MAPNAME                     9011
#define IDC_INDEX                       9012
#define IDC_LINE                        9014
#define IDC_TASKINFO                    9016
#define IDC_BUTTON2                     9017
#define IDC_NPC_NAME                    9018
#define IDC_MAPX                        9019
#define IDC_MAPY                        9020
#define IDC_BUTTON4                     9021
#define IDC_ROLE_NAME                   9021
#define IDC_ROLE_LV                     9023
#define IDC_MAP_NAME                    9024
#define IDC_MAP_XY                      9025
#define IDC_CASH                        9027
#define IDC_VOU                         9028
#define IDC_AUTO_                       9029
#define IDC_AUTO_TASK                   9029

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        9002
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         9030
#define _APS_NEXT_SYMED_VALUE           9001
#endif
#endif
