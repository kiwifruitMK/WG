#include "stdafx.h"
#include "Test.h"


//控件和npc相关测试
void TestFunc1() 
{
	char* szCtrlName = CallGetCurrentCtrlName();
	DWORD dwCartonState = CallGetCatonState();
	WRITE_LOG("当前控件名字:%s", szCtrlName);
	WRITE_LOG("动画状态:%d", dwCartonState);
	DWORD dwCtrlAddr = CallGetCtrlAddr("DramaDlg", "EscBtn");
	WRITE_LOG("动画控件指针:%X", dwCtrlAddr);
	if(dwCtrlAddr != -1)
	{
		WRITE_LOG("点击~");
		CallClickCtrl(dwCtrlAddr);
	}
	DWORD dwNpcAddr = CallGetRoundActor("文曲星");
	WRITE_LOG("NpcAddr:%X", dwNpcAddr);
	if(dwNpcAddr != -1){
		DWORD dwNpcID = CallGetRoundActorID(dwNpcAddr);
		WRITE_LOG("npcid:%X", dwNpcID);
		CallClickNpc(dwNpcID);
		DWORD dwTalkCtrlNpcID = CallGetTalkCtrlNpcID();
		if(dwTalkCtrlNpcID != -1){
			WRITE_LOG("talk ctrl  npcid:%X", dwTalkCtrlNpcID);
			char* szTxt= CallGetTalkCtrlItemTxtByIndex(1);
			WRITE_LOG("talk ctrl  item txt:%s", szTxt);
			if(szTxt != NULL){
				CallClickTalkCtrlItem(dwTalkCtrlNpcID, szTxt);
			}
		}
	}
	SNpcInfo Npc1;
	CallGetNpcInfo("文曲星", Npc1);
	DWORD dwNpc1ID = CallGetRoundActorID(Npc1.dwX, Npc1.dwY);
	WRITE_LOG("npc1id:%X", dwNpc1ID);
}

//更新人物信息相关测试
void TestFunc2() 
{
	CallUpdateRoleInfo();
	CallGetRoundMonster();
	DWORD dwNum, dwPos;
	bool bFlag = CallFindGoods("静心丹", dwNum, dwPos);
	WRITE_LOG("数量:%d", dwNum);
	WRITE_LOG("位置:%d", dwPos);
	WRITE_LOG("标识:%d", bFlag );
	CallLoadEquip(101, 0x1);
}