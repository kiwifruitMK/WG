#pragma once


// CBaseSet 对话框

class CBaseSet : public CDialog
{
	DECLARE_DYNAMIC(CBaseSet)

public:
	CBaseSet(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CBaseSet();

// 对话框数据
	enum { IDD = IDD_BASE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	// 秒循环
	void Loop_1_Sec(void);
	void UpdateRoleInfo(void);
	CString m_RoleName;
	CString m_RoleLV;
	CString m_MapName;
	CString m_Cash;
	CString m_Vou;
	CString m_XYPos;
	afx_msg void OnBnClickedAutoTask();
	BOOL m_AutoTask;
	void Loop_3_Sec(void);
	void AutoAddPoint(void);
};
