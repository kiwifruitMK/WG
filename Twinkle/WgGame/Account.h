#pragma once


// CAccount 对话框

class CAccount : public CDialog
{
	DECLARE_DYNAMIC(CAccount)

public:
	CAccount(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CAccount();

// 对话框数据
	enum { IDD = IDD_ACCOUNT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	BOOL m_IsAutoLogin;
	afx_msg void OnBnClickedCheck1();
	CString m_Account;
	CString m_Passwd;
	CString m_index;
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT_PTR nIDEvent);
	// 自动登录主逻辑
	void AutoLogin(void);
	CString m_Line;
	afx_msg void OnBnClickedButton1();
};
