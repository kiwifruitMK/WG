#include "stdafx.h"
#include "GameCall.h"
#include <queue>
#pragma warning(disable:4996) 

//登陆相关
char g_szAccount[] ="E474AB00";  //账号
char g_szPasswd[] = "DCE4AB00";  //密码
char g_szClickLogin[] = "FC22AB00"; //账号密码确认登陆按钮
char g_szLines[] ="ACF4AB00";  //分线对象地址
char g_szOklLine[]= "FC22AB00";  //玩家选择分线后的确认键对象
char g_szSelRole[] = "8468B200";   //登陆界面选角色对象地址
char g_szCheckOk[] = "FC22AB00";  //点击登陆验证按钮进入输入账号密码界面
//
DWORD s_dwLoginUI = 0x00ca11b8;  //登录ui界面状态基地址
DWORD s_dwEnter = 0x00c43434; //存放是否已经进入游戏

//任务相关
//特征码：8B 0D ?? ?? ?? ?? 56 E8 ?? ?? ?? ?? 8D 4C 24 48 C6 44 24 70 00
DWORD s_dwTaskBase = 0xCF1088; //任务基地址 特征码 + 2

//寻路相关
//特征码：8B 0D ?? ?? ?? ?? 8D 44 24 ?? 50 E8 ?? ?? ?? ?? 8B 0D ?? ?? ?? ?? E8 ?? ?? ?? ?? 85 FF
DWORD s_dwFindPathBase = 0xCECFF0;  //寻路基地址特征码 + 2
DWORD s_dwFindPathCall1 = 0x00535630;  //特征码: + 12
DWORD s_dwFindPathCall2 = 0x00537120; //特征码： + 16

//人物基础属性相关
//特征码：FF D0 8B 0D ?? ?? ?? ?? 8B 11 8B F0 8B 82 E4 00 00 00 FF D0 3B F0
DWORD s_dwRoleBase = 0xCF1AB8;  //特征码： + 0x04 人物属性基地址 



//人物状态 1 普通状态  2 老君状态  3.  战斗状态
// 特征码：6A 0? FF D2 C7 86 F4 01 00 00 0? 00 00 00 A1 ?? ?? ?? ?? 83 F8 0? 
DWORD s_dwRoleState = 0xC904A4; //特征码： + 0x10 

//当前面板
//特征码：8B 07 8B 90 ?? ?? 00 00 56 8B CF FF D2 85 C0 ?? ?? 8B 0D ?? ?? ?? ??

DWORD s_dwCurrentWindow = 0xCEE164;  //特征码 + 12

//当前是否在播放动画  0:普通界面 1：动画界面 ??
//特征码：53 53 89 2D ?? ?? ?? ?? E8 ?? ?? ?? ?? 8B 85 ?? ?? ?? ?? 83 C4 0?
DWORD s_dwCatonState = 0xCF62C4; //特征码 + 4

//控件遍历
//特征码：8B ?? ?? ?? ?? 00 8B 01 8B 90 28 01 00 00 56 57 FF D2
DWORD s_dwCtrlBase = 0xCEE164;    //特征码 + 0x2

//对话Npc
//特征码：6A ?? 68 ?? ?? ?? ?? 8B CF E8 ?? ?? ?? ?? 50 68 ?? ?? ?? ?? 56 E8 ?? ?? ?? ??
DWORD s_dwClickNpc = 0x006C82B0;   // call常量 + 16
// 对话Npc选项第一个参数常量
// 特征码：8B CB FF D0 85 C0 0F 84 ?? ?? ?? ?? 8B 13
DWORD s_dwSelItemConst = 0x0C05D29; // 第一个参数常量 + 15

//遍历周围场景npc和人
//特征码：8D BE ?? ?? ?? ?? 68 ?? ?? ?? ?? 8B CF E8 ?? ?? ?? ?? 8B 0D ?? ?? ?? ?? 50
DWORD s_dwRoundActor = 0xCF0FF0;  // 特征：+12


//获取地图Npc信息
//特征码：8B 0D ?? ?? ?? ?? 8D 54 24 38 52 E8 ?? ?? ?? ?? 8B F0 85 F6
DWORD s_dwMapNpcBase = 0xCF0DAC; //特征吗 + 2

//遍历怪物
//特征码：8B 8C 24 ?? ?? 00 00 51 8B 0D ?? ?? ?? ?? E8 ?? ?? ?? ?? 8B F8 3B FD
DWORD s_dwRoundMonster = 0xCEDCB0;  //特征码： + A

//遍历背包
//特征码：	CC 8B 0D ?? ?? ?? ?? 53 55 8B 6C 24 ?? 56 57
DWORD s_dwBagBase = 0xCEE368;  //特征码：+2

//人物加点
//特征码：6A 00 6A 00 68 ?? ?? ?? ?? 6A 00 68 ?? ?? ?? ?? E8 ?? ?? ?? ?? 83 C4 ?? 33 C0 5E C2 08 00 CC CC CC
//查找次数：第三次
DWORD s_dwAddPointCall1 = 0x006C82B0;  //特征码：-0x4
DWORD s_dwAddPointCall2 = 0x0062C020; //特征码：+ 0x12

//学习技能
//特征码：8D 4C 24 ?? E8 ?? ?? ?? ?? 50 68 ?? ?? ?? ?? 68 ?? ?? ?? ?? E8 ?? ?? ?? ?? 8B 0D ?? ?? ?? ?? 83 C4 ?? 85 C9
DWORD s_dwLeanSkillCall1 = 0x006C82B0;  //特征码：+ 0x15
//特征码: CC CC CC CC 8B 44 24 ?? 85 C0 ?? ?? 50 8B 44 24 ?? 50 68 ?? ?? ?? ?? 68 ?? ?? ?? ?? E8 ?? ?? ?? ?? 83 C4 ??
//搜索第一个，关键词confirm
DWORD s_dwLeanSkillCall2 = 0x006C82B0; //特征码: + 0x1D

// 遍历玩家身上的技能
//特征码：50 8B 82 ?? ?? ?? ?? FF D0 8b 0D ?? ?? ?? ?? 50 E8 ?? ?? ?? ?? c2 ?? ??
DWORD s_dwSkillBase = 0xCF0F20;  //call eax 下面是的

SRoleInfo RoleInfo;

void LogRecord(TCHAR*  szText)
{
	pMainDlg ->LogRecord(szText);
}

bool CallTypeAccountPasswd(DWORD dwCharacter, DWORD dwAddress)
{
	__try{
		__asm
		{
			mov ebx, dwCharacter
			push ebx
			mov ecx, dwAddress
			mov eax, 0x0044EA60
			call eax
		}
		return TRUE;
	}
	__except(1){
		LogRecord(_T("输入账号密码call失败，已经恢复~"));
		return false;
	}
	return true;
}

//点击登录按钮
bool CallClickLogin(DWORD dwAddress)
{
	__try{
		__asm
		{
				push    1 
				push    0x00AB1940 
				mov esi, dwAddress
				mov ecx, esi
				mov eax, 0x00435290
				call    eax
		}
	}
	__except(1){
		LogRecord(_T("点击登录call失败，已经恢复~"));
		return false;
	}
	return true;
}

bool CallSelLine(DWORD dwAddress)
{
	__try{
		__asm
		{
				push	0
				push	0x00ABF5E4 
				mov	esi, dwAddress
				mov	ecx, esi
				mov	eax, 0x00435290
				call    eax
		}
	}
	__except(1){
		LogRecord(_T("选择线路call失败，已经恢复~"));
		return false;
	}
	return true;
}

//选择线路后的确认键
bool CallOkLine(DWORD dwAddress)
{
	__try{
		__asm
		{
				push    1 
				push    0x00AB1940 
				mov esi, dwAddress
				mov ecx, esi
				mov eax, 0x00435290
				call    eax
		}
	}
	__except(1){
		LogRecord(_T("选择线路确认call失败，已经恢复~"));
		return false;
	}
	return true;
}

//选角色进入游戏
bool CallSelRoleAndLogin(DWORD dwAddress, DWORD dwIndexRole)
{
	__try{
		__asm
		{
			push	dwIndexRole
			mov	edi, dwAddress
			mov	ecx, edi
			mov	eax, 0x007CE700
			call		eax     //获取角色对象地址

			mov	ecx, [eax]
			push	0x0BBC9A9
			push   0x0BBC9C0
			mov	eax, 0x006D8920
			call		eax    //获取角色名字

			push	eax
			push   0x0B2680C
			push   0x1060
			mov    eax,  0x0069CE80
			call		eax
			add     esp, 0x0C
		}
		return true;
	}
	__except(1){
		LogRecord(_T("选角色进入游戏call失败，已经恢复~"));
		return false;
	}
	return true;
}

//登录界面状态
ELoginUI CallGetLoginUI()
{
	ELoginUI RtnValue = E_LOGIN_INVAILD;
	__try{
		__asm
		{
			mov eax, s_dwLoginUI
			mov eax, [eax]
			mov RtnValue, eax
		}
	}
	__except(1){
		LogRecord(_T("获取界面状态call失败，已经恢复~"));
		return E_LOGIN_INVAILD;
	}
	return RtnValue;
}


//是否登录到游戏
bool CallIsEnterGame()
{
	DWORD dwEnterFlag = -1;
	__try{
		__asm
		{
			mov eax,  s_dwEnter
			mov eax, [eax]
			mov dwEnterFlag, eax
		}
	}
	__except(1){
		LogRecord(_T("获取玩家是否已经进入游戏call失败，已经恢复~"));
	}
	if(dwEnterFlag == 1){
		return true;
	}else{
		return false;
	}
}

//点击登陆验证按钮进入输入账号密码界面
bool CallCheckOk(DWORD dwAddress)
{
	__try{
		__asm
		{
				push    1 
				push    0x00AB1940 
				mov esi, dwAddress
				mov ecx, esi
				mov eax, 0x00435290
				call    eax
		}
	}
	__except(1){
		LogRecord(_T("点击进入输入账号密码界面call失败，已经恢复~"));
		return false;
	}
	return true;
}

//寻路相关
bool CallFindPath(char * szDes, DWORD dwIsClickNPC)
{
	bool bRtnValue = true;
	__try{
		char buff [MAX_PATH] = {0};
		int iLen = strlen(szDes);
		if( iLen<= 14){
			memcpy(buff + 4, szDes, (size_t)iLen);
			*(UINT*)(buff + 20) = (UINT)iLen;
			*(buff + 24) = 15;
		}else{
			*(UINT*)(buff + 4) = (UINT)szDes;
			*(UINT*)(buff + 20) = (UINT)iLen;
			*(buff + 24) = 31;
		}
		DWORD dwTmp = (DWORD)buff;
		__asm
		{
			mov	eax, dwIsClickNPC
			push	eax

			mov	ecx, dword ptr [s_dwFindPathBase]
			mov	eax,  dwTmp
			push	eax

			mov	eax, s_dwFindPathCall1
			call		eax

			mov	ecx, dword ptr [s_dwFindPathBase]
			mov	eax, s_dwFindPathCall2
			call		eax
		}
	}
	__except(1){
		LogRecord(_T("寻路call失败，恢复~"));
		bRtnValue = false;
	}
	return true;
}
bool CallRunMap(CString strMapName, CString strMapX, CString strMapY)
{
	bool bRtnValue;
	CString strDes = strMapName + _T("(") +strMapX + _T(",") + strMapY + _T(")") ;
	char* szDes = strDes.GetBuffer();
	bRtnValue = CallFindPath(szDes,  0);
	return bRtnValue;
}
bool CallRunNPC(CString strNPCName)
{
	bool bRtnValue;
	char *szNPCName = strNPCName.GetBuffer();
	bRtnValue = CallFindPath(szNPCName,  1);
	return bRtnValue;
}


DWORD FindTree(DWORD dwEsi, const char* szType)
{
	DWORD dwRtnValue =  -1;
	std::queue<DWORD> dwQue;
	dwQue.push(dwEsi);
	while(! dwQue.empty())
	{
		DWORD dwFront = dwQue.front();
		dwQue.pop();
		//
		if(AddrToBYTE(dwFront + 0x45) == 0){
			char* szText = (char*)(dwFront + 0xc + 4);
			//WRITE_LOG("sztxt:%s", szText);
			if(strcmp(szText, szType) == 0){
				dwRtnValue = dwFront;
				break ;
			}else{
				DWORD dwLeft =  AddrToInt(dwFront);   //左结点
				DWORD dwRight =  AddrToInt(dwFront + 0x8);  //右结点
				dwQue.push(dwLeft);
				dwQue.push(dwRight);
			}
		}
	}
	return dwRtnValue;
}

bool GetAllTaskInfo(pSTaskInfo pTasks, int& iLen)
{
	bool bRtnValue = true;
	int i = 0;
	__try{
		DWORD dwBegin = AddrToInt(AddrToInt(s_dwTaskBase) + 0x88 + 0x4);
		DWORD dwEnd = AddrToInt(AddrToInt(s_dwTaskBase) + 0x88 + 0x8);
		while(dwBegin < dwEnd){
			DWORD dwTestValue = AddrToInt(dwBegin);
			if( 0 == dwTestValue){
				break;
			}
			dwTestValue += 0x4;  //[[[0CA06E0] + 88 + 1* 4]] + 4 
			dwTestValue += 0x4;  //[[[0CA06E0] + 88 + 1* 4]] + 4  + 4
			dwTestValue = AddrToInt(dwTestValue); // [[[[0CA06E0] + 88 + 1* 4]] + 4 +4]
			dwTestValue = AddrToInt(dwTestValue + 0x4) ; //[[[[[0CA06E0] + 88 + 1* 4]] + 4 +4] +4]=> esi
			//
			BYTE dwTestValue2 = AddrToBYTE(dwTestValue + 0x45);
			if(dwTestValue2 == 0 ){
				DWORD dwRtnValue1 = FindTree(dwTestValue, "task_type");
				char *szTaskType = (char*)AddrToInt(dwRtnValue1 + 0x2c);
				if(! IsBadReadPtr(szTaskType, 1024)){
					//
					DWORD dwRtnValue2 = FindTree(dwTestValue, "task_prompt");
					char *szPrompt = (char*)AddrToInt(dwRtnValue2 + 0x2c);
					//
					DWORD dwRtnValue3 = FindTree(dwTestValue, "task_tips");
					char *szTips = (char*)AddrToInt(dwRtnValue3 + 0x2c);
					//
					DWORD dwRtnValue7 = FindTree(dwTestValue, "alias");
					char *szAlias = (char*)AddrToInt(dwRtnValue7 + 0x2c);
					if(i < iLen){
						pTasks[i].strType = szTaskType;
						pTasks[i].strPrompt = szPrompt;
						pTasks[i].strTips = szTips;
						pTasks[i].strAlias = szAlias;
						i ++;
					}
				}
			}
			//
			dwBegin += 4; 
		}
	}
	__except(1){
		bRtnValue = false;
		LogRecord(_T("获取任务信息错误，恢复~"));
	}
	iLen = i;
	return bRtnValue;
}
bool CallGetAllTaskInfo(std::vector<STaskInfo> &TaskInfos)
{
	STaskInfo TaskInfo[100];
	int iLen = sizeof(TaskInfo) / sizeof(TaskInfo[0]);
	if(GetAllTaskInfo(TaskInfo, iLen))
	{
		for(int i = 0; i< iLen;  i++){
			TaskInfos.push_back(TaskInfo[i]);
		}
		return true;
	}else {
		return false;
	}
}

//人物属性相关
DWORD GetRoleInfo(const char* szType, DWORD dwTopBase)
{
	// ?[[[0CAC8E8]+0100+4+4] +4]
	// ? [[[0CAC8E8]+0100+4 + 4] + 4]
	DWORD dwExtraValue = 0;
	DWORD dwBaseValue = 0;
	//
	char szSpec1[] =  "max_life#max_mana#phy_power#mag_power#def#con#wiz#str#dex#speed";
	char szSpec2[] = "level#xpos#ypos#name#run_path#state";
	bool bSpec1 = strMember(szType, szSpec1);
	bool bSpec2 = strMember(szType, szSpec2);
	__try{
		if(bSpec2){   // 等级，坐标, 名字
			if(strcmp(szType, "level") == 0){
				return  AddrToInt(AddrToInt(s_dwRoleBase) + 0x4d8);
			}else if(strcmp(szType, "xpos") == 0){
				return  AddrToInt(AddrToInt(s_dwRoleBase) + 0x35c);
			}else if(strcmp(szType, "ypos") == 0){
				return  AddrToInt(AddrToInt(s_dwRoleBase) + 0x360);
			}else if(strcmp(szType, "name") == 0){
				return  AddrToInt(s_dwRoleBase) + 0x14c;
			}else if(strcmp(szType, "run_path") == 0){
				return  AddrToInt(AddrToInt(s_dwRoleBase) + 0x1ec);
			}else if(strcmp(szType, "state") == 0){
				return AddrToInt(s_dwRoleState);
			}else{
				return -1;
			}
		}else{  //其他
			DWORD dwRoleBase;
			if(0 == dwTopBase){
				 dwRoleBase= AddrToInt(AddrToInt(AddrToInt(s_dwRoleBase) + 0xc8+0x4+0x4) + 0x4);
			}else{
				dwRoleBase = dwTopBase;
			}
			DWORD dwBaseValueOb = FindTree(dwRoleBase, szType);
			if(strcmp(szType, "map_name") == 0){
				dwBaseValue = dwBaseValueOb + 0x2c;
			}else{
				dwBaseValue = ToInt((char*)(dwBaseValueOb + 0x2c));
			}
			if(bSpec1){ //比如最大血有装备加成
				DWORD dwRoleExtra = AddrToInt(AddrToInt(AddrToInt(s_dwRoleBase) + 0x100 + 0x4 + 04) + 4);
				DWORD dwExtraValueOb = FindTree(dwRoleExtra, szType);
				dwExtraValue= ToInt((char*)(dwExtraValueOb + 0x2c));
			}else{
				dwExtraValue  = 0;
			}
		}
	}
	__except(1){
		LogRecord(_T("获取人物信息错误，恢复~"));
		return -1;
	}
	return  dwExtraValue + dwBaseValue;
}

//更新人物信息
void CallUpdateRoleInfo()
{
	DWORD dwMaxHP = GetRoleInfo("max_life", 0);
	DWORD dwMaxMP = GetRoleInfo("max_mana", 0);
	DWORD dwHP = GetRoleInfo("life", 0);
	DWORD dwMP = GetRoleInfo("mana", 0);
	DWORD dwXPos =  GetRoleInfo("xpos", 0);
	DWORD dwYPos =  GetRoleInfo("ypos", 0);
	DWORD dwLevel =  GetRoleInfo("level", 0);
	char* szName =  (char*)GetRoleInfo("name", 0);
	DWORD dwCash =  GetRoleInfo("cash", 0);
	DWORD dwVoucher = GetRoleInfo("voucher", 0);
	char* szMapName = (char*)GetRoleInfo("map_name", 0);
	DWORD dwRunPath = GetRoleInfo("run_path", 0);
	DWORD dwState = GetRoleInfo("state", 0);
	DWORD dwRemainPoint = GetRoleInfo("attrib_point",  0);
	DWORD dwPolar = GetRoleInfo("polar",  0);
	RoleInfo.dwMaxMP = dwMaxHP;
	RoleInfo.dwMaxMP = dwMaxMP;
	RoleInfo.dwHP = dwHP;
	RoleInfo.dwMP = dwMP;
	RoleInfo.dwX = dwXPos;
	RoleInfo.dwY = dwYPos;
	RoleInfo.dwLevel = dwLevel;
	strcpy(RoleInfo.szName, szName);
	RoleInfo.dwCash = dwCash;
	RoleInfo.dwVoucher = dwVoucher;
	strcpy(RoleInfo.szMapName, szMapName);
	RoleInfo.dwRunPath = dwRunPath;
	RoleInfo.dwState = dwState;
	RoleInfo.dwRemainPoint = dwRemainPoint;
	RoleInfo.dwPolar = dwPolar;
	//
// 	WRITE_LOG("最大血值:%d", dwMaxHP);
//  	WRITE_LOG("最大蓝值:%d", dwMaxMP);
//  	WRITE_LOG("当前血值:%d", dwHP);
//  	WRITE_LOG("当前蓝值:%d", dwMP);
//  	WRITE_LOG("当前X:%d", dwXPos);
//  	WRITE_LOG("当前Y:%d", dwYPos);
//  	WRITE_LOG("当前等级:%d", dwLevel);
//  	WRITE_LOG("名字:%s", szName);
// 	WRITE_LOG("金钱:%d", dwCash);
 //	WRITE_LOG("代金券:%d", dwVoucher);
// 	WRITE_LOG("地图名:%s", szMapName);
// 	WRITE_LOG("是否寻路:%d", dwRunPath);
// 	WRITE_LOG("状态:%d", dwState);
//		WRITE_LOG("剩余的点数:%d", dwRemainPoint);
//		WRITE_LOG("职业:%d", dwPolar);
}

//获取当前控件名字
char* CallGetCurrentCtrlName()
{
	//db  [[0CAB7E4] + 01e4 + 010] + 094
	char *szCurrentWindow = NULL;
	__try{
		szCurrentWindow = (char*)(AddrToInt(AddrToInt(s_dwCurrentWindow) + 0x1e4 + 0x10) + 0x094);
	}
	__except(1){
		LogRecord(_T("获取当前窗口失败，恢复~"));
	}
	return  szCurrentWindow;
}

//获取当前是否在剧情动画界面， 0：普通界面  1：动画界面
DWORD CallGetCatonState()
{
	//dd [00cb10ec] + 40
	DWORD dwCatonState =  -1;
	__try{
		dwCatonState = AddrToInt(AddrToInt(s_dwCatonState) + 0x40);
	}
	__except(1){
		LogRecord(_T("获取当前窗口失败，恢复~"));
	}
	return  dwCatonState;
}

//获取控件指针
DWORD CallGetCtrlAddr(char* szCtrlName, DWORD dwTopWnd)
{
	DWORD dwRtnValue = -1;
	__try{
		if(0 == dwTopWnd){
			dwTopWnd = AddrToInt(s_dwCtrlBase);
		}else{
			//nothing
		}
		DWORD dwLink = AddrToInt(dwTopWnd + 0x1e4);
		DWORD dwNum = AddrToInt(dwTopWnd + 0x1e8);
		for(DWORD i = 0; i < dwNum;  i++){
			dwLink = AddrToInt(dwLink);
			DWORD dwCtrAddr = AddrToInt(dwLink + 0x8);
			char* szName =  (char*)(dwCtrAddr + 0x94);
			if(strcmp(szCtrlName,  szName) == 0){
				return dwCtrAddr;
			}
		}
	}
	__except(1){
		LogRecord(_T("查找控件指针报错，恢复~"));
	}
	return dwRtnValue;
}
DWORD CallGetCtrlAddr(char* szParent, char* szChild)
{
	DWORD dwTop  = CallGetCtrlAddr(szParent, (DWORD)0);
	if(-1 == dwTop){
		return -1;
	}else{
		return CallGetCtrlAddr(szChild, dwTop);
	}
}


// 点击控件
bool CallClickCtrl(DWORD dwCtrlAddr)
{
	bool bRtnValue = true;
	__try{
		__asm
		{
			mov	esi,		dwCtrlAddr
			mov	eax,		[esi]
			mov	edx,		[eax+0x80]
			push	1
			push	1
			push	0
			mov	ecx,		esi
			call		edx
		}
	}
	__except(1){
		LogRecord(_T("点击控件报错，恢复~"));
		bRtnValue = false;
	}
	return bRtnValue;
}

// 返回周围Actor信息
DWORD CallGetRoundActor(char* szActorName)
{
	DWORD dwRtnValue = -1;
	DWORD dwLink = AddrToInt(AddrToInt(s_dwRoundActor) + 0x28);
	DWORD dwNum =  AddrToInt(AddrToInt(s_dwRoundActor) + 0x28 + 4);
	//ERROR_MSG("数量:%d", dwNum);
	__try{
		for(DWORD i = 0; i< dwNum; i++){
			dwLink = AddrToInt(dwLink);
			DWORD dwActorAddr = AddrToInt(dwLink + 0x8);
			DWORD dwID = CallGetRoundActorID(dwActorAddr);
			if(strcmp(szActorName, (char*)(dwActorAddr + 0x14c)) == NULL){
				return CallGetRoundActorID(dwActorAddr);
			}
		}
	}
	__except(1){
		LogRecord(_T("获取周围NPC信息报错，恢复~"));
		return dwRtnValue;
	}
	return dwRtnValue;
}

//根据Actor地址获取ID
DWORD CallGetRoundActorID(DWORD dwActorAddr)
{
	DWORD dwRtnValue = -1;
	__try{
		dwRtnValue = AddrToInt(dwActorAddr + 0x8);
	}
	__except(1){
		LogRecord(_T("获取周围NpcID，恢复~"));
	}
	return dwRtnValue;
}

 //根据Actor的x,y，获取ActorID
DWORD CallGetRoundActorID(DWORD dwDesX, DWORD dwDesY)
{
	DWORD dwRtnValue = -1;
	DWORD dwLink = AddrToInt(AddrToInt(s_dwRoundActor) + 0x28);
	DWORD dwNum =  AddrToInt(AddrToInt(s_dwRoundActor) + 0x28 + 4);
	//ERROR_MSG("数量:%d", dwNum);
	__try{
		for(DWORD i = 0; i< dwNum; i++){
			dwLink = AddrToInt(dwLink);
			DWORD dwActorAddr = AddrToInt(dwLink + 0x8);
			DWORD dwX = AddrToInt( dwActorAddr + 0x35c);
			DWORD dwY = AddrToInt(dwActorAddr + 0x360);
			//DWORD dwType = GetRoleInfo("type", dwActorAddr);  //1. 玩家 2.宝宝  4.可兑换Npc
			if(dwDesX == dwX && dwDesY == dwY /*&& 4 == dwType*/){
				//ERROR_MSG("x:%d, y:%d", dwX, dwY);
				dwRtnValue = AddrToInt(dwActorAddr + 0x8);
				break;
			}
		}
	}
	__except(1){
		LogRecord(_T("根据Actor的x,y，获取ActorID，恢复~"));
		return dwRtnValue;
	}
	return dwRtnValue;
}

//点击对话NPC
bool CallClickNpc(DWORD dwNpcID)
{
	char szTxt[]  = "id = %d";
	DWORD dwTxt = (DWORD)szTxt;
	bool bRtnValue = true;
	__try{
		__asm
		{
			push	dwNpcID
			mov    eax,	dwTxt
			push	eax
			push	0x1036
			mov	eax,  s_dwClickNpc
			call		eax
			add     esp, 0xc
		}
	}
	__except(1){
		bRtnValue = false;
		LogRecord(_T("点击兑换Npc，恢复~"));
	}
	return bRtnValue;
}

//获取Npc对话框控件指针 TalkMenuDlg
DWORD CallGetNpcTalkCtrlAddr()
{
	return CallGetCtrlAddr("TalkMenuDlg", (DWORD)0);
}

//获取Npc对话控件窗口的NpcID
DWORD CallGetTalkCtrlNpcID()
{
	DWORD dwRtnValue = -1;
	__try{
		DWORD dwTalkCtrlAddr = CallGetNpcTalkCtrlAddr();
		if(dwTalkCtrlAddr != -1){
			dwRtnValue =  AddrToInt(dwTalkCtrlAddr + 0x354);
		}else{
			//nothing
		}
	}
	__except(1){
		LogRecord(_T("获取Npc对话窗口的NpcID，恢复~"));
	}
	return dwRtnValue;
}

// 获取当前对话框第index个项文本
char*  CallGetTalkCtrlItemTxtByIndex(DWORD dwIndex)
{
	char* szRtnValue = NULL;
	DWORD dwCtrlAddr = CallGetNpcTalkCtrlAddr();
	if(-1 != dwCtrlAddr){
		__try{
			DWORD dwLink = AddrToInt(dwCtrlAddr + 0x398);
			DWORD dwNum = AddrToInt(dwCtrlAddr + 0x39c);
			//ERROR_MSG("LinkAddr:%X, Num:%d", dwLink, dwNum);
			for(DWORD i  = 0; i< dwNum; i++){
				dwLink = AddrToInt(dwLink);
				DWORD dwCtrlAddr = AddrToInt(dwLink + 0x8);
				if(dwIndex == i + 1){
					//ERROR_MSG("item:%s, ctrladdr:%X",(char*)(dwCtrlAddr + 0x204), dwCtrlAddr);
					szRtnValue  = (char*)(dwCtrlAddr + 0x204);
					break;
				}
			}
		}
		__except(1){
			LogRecord(_T("获取当前对话框第index个项文本，恢复~"));
		}
	}else{
		//nothing
	}
	return szRtnValue;
}

// 点击Ncp对话的某一项
bool CallClickTalkCtrlItem(DWORD dwNpcID, char* szTxt)
{
	bool bRtnValue = false;
	DWORD dwTxt = (DWORD) szTxt;
	char szFormat[] = "id = %d, menu_item = %s, para = %s";
	DWORD dwFormat = (DWORD)szFormat;
	__try{
		__asm
		{
				push	s_dwSelItemConst
				push	dwTxt
				push	dwNpcID
				push	dwFormat
				push	0x3038
				mov	eax,	s_dwClickNpc
				call		eax
				add		esp,	0x14
		}
	}
	__except(1){
		LogRecord(_T("点击Ncp对话的某一项，恢复~"));
	}
	return bRtnValue;
}

// 获取一个Npc的信息
bool CallGetNpcInfo(CString strNpcName, SNpcInfo &NpcInfo)
{
	char *szNPCName = strNpcName.GetBuffer();
	bool bRtnValue = CallGetNpcInfo(szNPCName, NpcInfo);
	return bRtnValue;
}
// 获取一个Npc的信息
bool CallGetNpcInfo(char *szNpcName, SNpcInfo &NpcInfo)
{
	//DD [[[[[00CCFC5C]+18]+i*4]+8]+4]+02c
	bool bRtnValue = false;
	__try{
		DWORD dwBeginAddr = AddrToInt(AddrToInt(s_dwMapNpcBase) + 0x18);
		DWORD dwEndAddr = AddrToInt(AddrToInt(s_dwMapNpcBase) + 0x1C);
		while(dwBeginAddr < dwEndAddr){
			DWORD  dwDataAddr = AddrToInt(dwBeginAddr);
			char *szNpcTxt = (char*)(AddrToInt(AddrToInt(dwDataAddr + 0x8) + 0x4) + 0x2c);
			if(strcmp(szNpcName, szNpcTxt) == NULL){
				//ERROR_MSG("Npc名字:%s", szNpcTxt);
				DWORD dwTreeDataAddr = AddrToInt(AddrToInt(dwDataAddr + 0x8) + 0x4);
				DWORD dwInsideBase = FindTree(dwTreeDataAddr, "inside");
				DWORD dwOutsideBase = FindTree(dwTreeDataAddr, "outside");
				DWORD dwX = FindTree(dwTreeDataAddr, "x");
				DWORD dwY = FindTree(dwTreeDataAddr, "y");
// 				ERROR_MSG("inside:%s", (char*)(dwInsideBase + 0x2c));
// 				ERROR_MSG("outside:%s", (char*)(dwOutsideBase + 0x2c));
// 				ERROR_MSG("x:%s", (char*)(dwX + 0x2c));
// 				ERROR_MSG("y:%s", (char*)(dwY + 0x2c));
				strcpy(NpcInfo.szNpcName, szNpcName);
				strcpy(NpcInfo.szLMapName, (char*)(dwOutsideBase + 0x2c));
				strcpy(NpcInfo.szSMapName, (char*)(dwInsideBase + 0x2c));
				NpcInfo.dwNpcAddr = dwDataAddr;
				NpcInfo.dwX = ToInt((char*)(dwX + 0x2c));
				NpcInfo.dwY = ToInt((char*)(dwY + 0x2c));
				bRtnValue = true;
				break;
			}
			dwBeginAddr += 0x4;
		}
	}
	__except(1){
		LogRecord(_T("获取一个Npc的信息，恢复~"));
	}
	return bRtnValue;
}

//遍历周围怪物信息
void CallGetRoundMonster()
{
	__try{
		DWORD dwBeginAddr = AddrToInt(AddrToInt(s_dwRoundMonster) + 0x258 + 4);
		DWORD dwEndAddr = AddrToInt(AddrToInt(s_dwRoundMonster) + 0x258 + 4 + 4);
		while(dwBeginAddr < dwEndAddr){
			DWORD dwMonsterAddr = AddrToInt(dwBeginAddr);
			DWORD dwMonsterID = AddrToInt(dwMonsterAddr + 0x8);
			char *szMonsterName = (char*)(dwMonsterAddr + 0x14c);
			if(dwMonsterID != 0){
				WRITE_LOG("dwMonsterAddr:%X, monsterid:%x, monstername:%s", dwMonsterAddr, dwMonsterID, szMonsterName);
			}
			dwBeginAddr += 4; 
		}
	}
	__except(1){
			LogRecord(_T("遍历周围怪物信息，恢复~"));
	}
}

//遍历背包树
bool FindGoodsTree(DWORD dwTree,  char* szNeeFind, DWORD &dwNum, DWORD &dwPos)
{
	bool bRtnValue =  false;
	std::queue<DWORD> dwQue;
	dwQue.push(dwTree);
	while(! dwQue.empty())
	{
		DWORD dwFront = dwQue.front();
		dwQue.pop();
		//
		if(AddrToBYTE(dwFront + 0x15) == 0){
			DWORD dwGoodsAddr1 = AddrToInt(dwFront + 0x10);
			DWORD dwGoodsAddr2 = AddrToInt(AddrToInt(dwGoodsAddr1 + 0xC) + 0x4);
			DWORD dwNameAddr = FindTree(dwGoodsAddr2, "name");
			char* szGoodsName = (char*)(dwNameAddr + 0x2C);
			//ERROR_MSG("goodsname:%s", szGoodsName);
			if(strcmp(szNeeFind, szGoodsName) == NULL){
				DWORD dwPosAddr = FindTree(dwGoodsAddr2, "pos");
				char *szPos =  (char*)(dwPosAddr + 0x2C);
				//
				DWORD dwAmountAddr = FindTree(dwGoodsAddr2, "amount");
				char *szAmountAddr =  (char*)(dwAmountAddr + 0x2C);
				//
				dwNum = ToInt(szAmountAddr);
				dwPos = ToInt(szPos);
				
				bRtnValue = true;
				break ;
			}else{
				DWORD dwLeft =  AddrToInt(dwFront);   //左结点
				DWORD dwRight =  AddrToInt(dwFront + 0x8);  //右结点
				dwQue.push(dwLeft);
				dwQue.push(dwRight);
			}
		}
	}
	return bRtnValue;
}
//根据物品名字找物品指针
bool CallFindGoods(char* szNeedFind, DWORD &dwNum, DWORD &dwPos)
{
	bool bRtnValue = false;
	__try{
		DWORD dwBagTree = AddrToInt(AddrToInt(s_dwBagBase) + 0x18);
		DWORD dwBagPosNum = AddrToInt(AddrToInt(s_dwBagBase) + 0x18 + 0x4);  //背包占用格子数量
		DWORD dwBagPosTree = AddrToInt(dwBagTree + 0x4);
		bRtnValue = FindGoodsTree(dwBagPosTree, szNeedFind, dwNum, dwPos);
	}
	__except(1){
			LogRecord(_T("根据物品名字找物品指针，恢复~"));
	}
	return bRtnValue;
}

//穿装备, 位置：0x1武器、0x2头、0x3衣服、0x33腰带、鞋子0xA
bool CallLoadEquip (DWORD dwPos, DWORD dwPart)
{
	bool bRtnValue = false;
	char szTip[] = "pos = %d, equip_part = %d";
	DWORD dwTip = (DWORD)szTip;
	__try{
		__asm
		{
			push dwPart
			push dwPos
			push dwTip
			push 0x1028
			mov eax, s_dwClickNpc
			call	eax
			add esp, 0x10
		}
		bRtnValue = true;
	}
	__except(1){
			LogRecord(_T("穿装备，恢复~"));
	}
	return bRtnValue;
}

//获取角色等级
DWORD CallRoleLevel()
{
	return GetRoleInfo("level", 0);
}

//加点call
bool CallAddPoint(DWORD dwDex,  DWORD dwStr, DWORD dwWiz, DWORD dwCon)
{
	bool bRtnValue = false;
	char szTxt1[] = "id = %d, con_count = %d, wiz_count = %d, str_count = %d, dex_count = %d";
	DWORD dwTxt1 = (DWORD)szTxt1;
	char szTxt2[] = "CLOSE_CTM";
	DWORD dwTxt2 = (DWORD)szTxt2;
	char szTxt3[] = "RecommendAssignAttribDlg";
	DWORD dwTxt3 = (DWORD)szTxt3;
	__try
	{
		__asm
		{
			push dwDex
			push dwStr
			push dwWiz
			push dwCon

			push 0
			push dwTxt1
			push  0x03804

			mov eax, s_dwAddPointCall1
			call eax

			push 0
			push 0
			push dwTxt2
			push 0 
			push dwTxt3

			mov eax, s_dwAddPointCall2
			call eax
			add     esp,  0x30

		}
		bRtnValue = true;
	}
	__except(1){
		LogRecord(_T("加点call，恢复~"));
	}
	return bRtnValue;
}

//学习技能
bool CallLeanSkill(DWORD dwSkillID, DWORD dwNPCID, DWORD dwUpLV)
{
	bool bRtnValue = true;
	char szTxt1[] = "id = %d, student_id = %d, no = %d";
	DWORD dwTxt1= (DWORD)szTxt1;
	char szTxt2[] = "confirm = %d, info = %s";
	DWORD dwTxt2= (DWORD)szTxt2;

	char szUpLV[MAX_PATH] = {0};
	itoa(dwUpLV, szUpLV, 10);
	DWORD dwUpLVAddr = (DWORD)szUpLV;
	//
	__try
	{
		__asm
		{
			push dwSkillID
			mov eax, s_dwRoleBase
			mov eax, [eax]
			mov eax, [eax + 8]
			push eax
			push dwNPCID
			push dwTxt1
			push 0x2074

			mov eax, s_dwLeanSkillCall1
			call    eax
			add   esp, 0x14
			//
			push dwUpLVAddr
			push 1
			push dwTxt2
			push  0x0510A
			mov eax, s_dwLeanSkillCall2
			call eax
			add esp, 0x010
		}
	}
	__except(1){
		bRtnValue = false;
		LogRecord(_T("学习技能，恢复~"));
	}
	return bRtnValue;
}

//遍历技能
bool GetAllSkill(std::vector<SSkill> & skills)
{
		bool bRtnValue =  false;
		DWORD dwTop = AddrToInt (AddrToInt (AddrToInt (s_dwSkillBase) + 0x24)+0x4);
		//
		std::queue<DWORD> tree_que;
		tree_que.push(dwTop);
		int iCount  = 0;
		while(! tree_que.empty() && iCount < 1000)
		{
			DWORD dwTree = tree_que.front();
			tree_que.pop();
			//
			iCount ++;
			DWORD dwFlag = AddrToBYTE(dwTree + 0x15);
			if(0 == dwFlag){
				DWORD dwSkillAddr1 = AddrToInt(dwTree + 0x10);
				DWORD dwSkillAddr2 = AddrToInt ( AddrToInt (dwSkillAddr1 + 0x8) + 0x4);
				DWORD dwNameAddr = FindTree(dwSkillAddr2, "skill_name");
				if(-1 != dwNameAddr){
					DWORD dwLVAddr = FindTree(dwSkillAddr2, "skill_level");
					DWORD dwCostAddr = FindTree(dwSkillAddr2, "skill_mana_cost");
					SSkill skill;
					strcpy(skill.szSkillName, (char*)dwNameAddr + 0x2c);
					skill.dwSkillLV = ToInt((char*)(dwLVAddr + 0x2c));
					skill.dwManaCost = ToInt((char*)(dwCostAddr + 0x2c));
					skills.push_back(skill);
				}
				//
				DWORD dwLeft = AddrToInt(dwTree + 0x0);
				DWORD dwRight = AddrToInt(dwTree + 0x8);
				tree_que.push(dwLeft);
				tree_que.push(dwRight);
			}
		}
		return iCount < 1000;
}
bool CallGetAllSkill(std::vector<SSkill> & skills) 
{
	bool  bRtnValue = true;
	__try{
		bRtnValue = GetAllSkill(skills);
	}
	__except(1){
		bRtnValue = false;
		LogRecord(_T("遍历技能, 已经恢复"));
	}
	return bRtnValue;
}