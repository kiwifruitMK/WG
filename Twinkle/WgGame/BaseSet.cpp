// BaseSet.cpp : 实现文件
//

#include "stdafx.h"
#include "WgGame.h"
#include "BaseSet.h"
#include "afxdialogex.h"
#include "AutoTask.h"


// CBaseSet 对话框

IMPLEMENT_DYNAMIC(CBaseSet, CDialog)

CBaseSet::CBaseSet(CWnd* pParent /*=NULL*/)
	: CDialog(CBaseSet::IDD, pParent)
	, m_RoleLV(_T(""))
	, m_MapName(_T(""))
	, m_Cash(_T(""))
	, m_Vou(_T(""))
	, m_XYPos(_T(""))
{

	m_RoleName = _T("");
}

CBaseSet::~CBaseSet()
{
}

void CBaseSet::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_ROLE_NAME, m_RoleName);
	DDX_Text(pDX, IDC_ROLE_LV, m_RoleLV);
	DDX_Text(pDX, IDC_MAP_NAME, m_MapName);
	DDX_Text(pDX, IDC_CASH, m_Cash);
	DDX_Text(pDX, IDC_VOU, m_Vou);
	DDX_Text(pDX, IDC_MAP_XY, m_XYPos);
	DDX_Check(pDX, IDC_AUTO_TASK, m_AutoTask);
}


BEGIN_MESSAGE_MAP(CBaseSet, CDialog)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_AUTO_TASK, &CBaseSet::OnBnClickedAutoTask)
END_MESSAGE_MAP()




// 秒循环
void CBaseSet::Loop_1_Sec(void)
{
	UpdateRoleInfo();
	AutoAddPoint();
	
}


void CBaseSet::UpdateRoleInfo(void)
{
	char *szCurrentCtrlname = CallGetCurrentCtrlName();
	if(szCurrentCtrlname != NULL && strcmp(szCurrentCtrlname, "ChatEditDlg") == NULL){
		CallUpdateRoleInfo();
		m_RoleName = RoleInfo.szName;
		m_RoleLV.Format(_T("%d"), RoleInfo.dwLevel);
		m_MapName = RoleInfo.szMapName;
		m_XYPos.Format(_T("%d,%d"), RoleInfo.dwX, RoleInfo.dwY);
		m_Cash.Format(_T("%d"), RoleInfo.dwCash);
		m_Vou.Format(_T("%d"), RoleInfo.dwVoucher);
		UpdateData(FALSE);
	}
}


void CBaseSet::OnBnClickedAutoTask()
{
	UpdateData(TRUE);
	if(m_AutoTask){
		DWORD dwLevel = CallRoleLevel();
		if(dwLevel > 0 && dwLevel <= 9){
			AutoTask(1);
			WRITE_LOG("开始练级 1~9 ~");
		}else if(dwLevel >= 10 && dwLevel <= 19){
			AutoTask(2);
			WRITE_LOG("开始练级 10~19 ~");
		}else{
			//nothing
		}
	}else{
		AutoTask(0);
		WRITE_LOG("结束练级");
	}
}


void CBaseSet::Loop_3_Sec(void)
{
	LoopTask();
}

//自动加点
void CBaseSet::AutoAddPoint(void)
{
	char *szCurrentCtrlname = CallGetCurrentCtrlName();
	if(szCurrentCtrlname != NULL && strcmp(szCurrentCtrlname, "ChatEditDlg") == NULL){
		if(RoleInfo.dwRemainPoint > 0){
			WRITE_LOG("自动加点， 职业:%d",  RoleInfo.dwPolar);
			char szPoloar[10] = {0};
			_itoa_s(RoleInfo.dwPolar, szPoloar, 10);
			//
			tinyxml2::XMLDocument doc;  
			tinyxml2::XMLError eError = doc.LoadFile(szAppCfgPath);
			WRITE_LOG("读取配置返回值:%d",  eError);
			if (tinyxml2::XML_SUCCESS == eError){
				tinyxml2::XMLElement *lanuch = doc.RootElement();
				tinyxml2::XMLElement * role_cfg = lanuch->FirstChildElement("role_cfg");
				tinyxml2::XMLElement  *add_point = role_cfg->FirstChildElement("add_point");
				while(add_point)
				{
					const char *polar = add_point->Attribute("polar");
					if(strcmp(polar, szPoloar) ==  NULL){
						tinyxml2::XMLElement  *dex = add_point->FirstChildElement("dex");
						tinyxml2::XMLElement  *str = add_point->FirstChildElement("str");
						tinyxml2::XMLElement  *wiz = add_point->FirstChildElement("wiz");
						tinyxml2::XMLElement  *con = add_point->FirstChildElement("con");
						WRITE_LOG(
							"自动加点，敏捷:%s, 力量:%s, 灵力:%s,体质:%s", 
							dex->GetText(), 
							str->GetText(), 
							wiz->GetText(),
							con->GetText());
						CallAddPoint(ToInt(dex->GetText()), ToInt(str->GetText()), ToInt(wiz->GetText()), ToInt(con->GetText()));
						break;
					}else{
						add_point = add_point ->NextSiblingElement();
					}
				}
			}
		}
	}
}
