#define MYHOOKAPI extern "C" __declspec(dllexport)

#include "stdafx.h"
#include "../CommonLib/MyLog.h"
#include "GameHook.h"
#include "WgGame.h"
#include "MainDlg.h"

extern CWgGameApp theApp;
extern CMainDlg *pMainDlg;
extern HHOOK hHook;

LRESULT CALLBACK KeyboardProc(int nCode, WPARAM wParam, LPARAM lParam)
{
	BOOL bKeyUp = lParam & (1 << 31);
	if (bKeyUp && wParam == VK_HOME && nCode == HC_ACTION) {
	
		if (pMainDlg == NULL) //判断对象是否已被创建
		{
			AFX_MANAGE_STATE(AfxGetStaticModuleState());
			CWnd *pCWnd = CWnd::GetForegroundWindow();
			//当前窗口是否是游戏窗口
			 CString strWndClass;
			::GetClassName(pCWnd->GetSafeHwnd(), strWndClass.GetBufferSetLength(MAX_PATH + 1), MAX_PATH);
			if(strWndClass.Find(CLIENT_CLASS_NAME) >=0){
				pMainDlg = new CMainDlg();
				pMainDlg -> Create(IDD_MAINDLG, pCWnd);
				pMainDlg -> ShowWindow(SW_SHOW);
			}
		}else {
			//根据当前呼出窗口的状态来显示或隐藏呼出窗口
			pMainDlg->ShowWindow(pMainDlg->IsWindowVisible() ? SW_HIDE : SW_SHOW);
		}
		wParam=VK_TAB;
	}
	return ::CallNextHookEx(hHook, nCode, wParam ,lParam);
}

MYHOOKAPI BOOL WINAPI InstallHook(HWND hwnd)  //安装钩子
{
	WRITE_LOG("========================\r\n 安装钩子......\r\n");
	WRITE_LOG("句柄是 = 0x%08X \r\n",hwnd);
	DWORD pid,threadid;
	threadid = GetWindowThreadProcessId((HWND)hwnd,&pid);
	WRITE_LOG("进程PID = %d, 主线程ID = 0x%08X \r\n",pid,threadid);
	hHook = ::SetWindowsHookEx(WH_KEYBOARD, &KeyboardProc,theApp.m_hInstance,threadid );
	if (hHook != NULL)
	{
		WRITE_LOG("键盘钩子安装成功，句柄是：0x%08X \r\n",hHook);
		return TRUE;
	}
	else
	{
		WRITE_LOG("键盘钩子安装失败 \r\n");
		return FALSE;
	}
}


MYHOOKAPI BOOL WINAPI UninstallHook()  //卸载钩子
{
	return ::UnhookWindowsHookEx(hHook);
}
