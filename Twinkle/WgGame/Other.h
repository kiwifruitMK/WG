#pragma once


// COther 对话框

class COther : public CDialog
{
	DECLARE_DYNAMIC(COther)

public:
	COther(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~COther();

// 对话框数据
	enum { IDD = IDD_OTHER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	CString m_NPCName;
	CString m_MapName;
	CString m_MapX;
	afx_msg void OnBnClickedButton4();
	CString m_MapY;
	afx_msg void OnBnClickedButton3();
};
