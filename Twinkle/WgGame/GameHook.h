#ifndef _H_GAME_HOOK_H_
#define _H_GAME_HOOK_H_

#ifndef MYHOOKAPI
#define MYHOOKAPI extern "C" __declspec(dllimport)
#endif
MYHOOKAPI BOOL WINAPI InstallHook(HWND hwnd);
MYHOOKAPI BOOL WINAPI UninstallHook();
#endif