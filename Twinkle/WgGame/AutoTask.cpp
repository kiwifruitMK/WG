#include "stdafx.h"
#include "AutoTask.h"
#include "GameCall.h"

DWORD s_dwAuto_1_9 = 0;   //1 ~ 9自动任务
DWORD s_dwAuto_10_19= 0; //10 ~ 19 自动任务

// 自动任务
void AutoTask(DWORD dwTaskTyep)
{
	switch(dwTaskTyep)
	{
	case 0:
		s_dwAuto_1_9 = 0;
		s_dwAuto_10_19 = 0;
		break;
	case 1: 
		s_dwAuto_1_9 = 1;
		s_dwAuto_10_19 =0;
		break;
	case 2:
		s_dwAuto_1_9 = 0;
		s_dwAuto_10_19 =1;
		break;
	default:
		break;
	}
}

// 去掉无用的字符
CString TrimUnUse(CString strPropt)
{
	strPropt.Replace(_T("#n"), _T(""));
	strPropt.Replace(_T("#R"), _T(""));
	strPropt.Replace(_T("#P"), _T(""));
	strPropt.Replace(_T("#Y"), _T(""));
	strPropt.Replace(_T("#Z"), _T(""));
	strPropt.Replace(_T("|"), _T(""));
	return strPropt;
}


//任务中寻访npc,如果返回false，代表在寻访中，返回true代表已经寻访到了
bool TaskCallNpc(CString strNpcName, DWORD &dwOutNpcID)
{
	DWORD dwTalkNpcID = CallGetTalkCtrlNpcID();
	if( dwTalkNpcID <= 0){
		CallRunNPC(strNpcName);
		WRITE_LOG("继续寻路~");
		return false;
	}else{
		SNpcInfo NpcInfo;
		if(CallGetNpcInfo(strNpcName, NpcInfo)){
			DWORD dwFindedNpc = CallGetRoundActorID(NpcInfo.dwX, NpcInfo.dwY);
			if(dwTalkNpcID == dwFindedNpc){
				WRITE_LOG("找到npc:%X", dwTalkNpcID);
				dwOutNpcID = dwTalkNpcID;
				return true;
			}else{
				WRITE_LOG("没有找到npc:%X", dwTalkNpcID);
				CallRunNPC(strNpcName);
				return false;
			}
		}else{
			WRITE_LOG("没有找到Npc信息~");
			CallRunNPC(strNpcName);
			return false;
		}
	}
}

void TaskLoadEquip(DWORD dwPart)
{
	for(DWORD i = 101; i <= 120; i ++){
		CallLoadEquip(i, dwPart);
	}
}

//问道小子面板处理
void AskTaoXiaoZhi()
{
	DWORD dwNpcID = CallGetTalkCtrlNpcID();
	if(0x13878 == dwNpcID){
		char *szTxt1 = CallGetTalkCtrlItemTxtByIndex(1);
		CallClickTalkCtrlItem(dwNpcID, szTxt1);
	}
}

// 1 ~ 9
void Do_1_9_Task(STaskInfo NeedDo)
{
	CString strPropt = NeedDo.strPrompt;
	CString strAlias = NeedDo.strAlias;
	strPropt = TrimUnUse(strPropt);
	DWORD dwNpcID;
	//MessageBox(NULL, strPropt, NULL, MB_OK);
	if(strPropt.Find(_T("寻访王老板")) > 0){
		TaskCallNpc(_T("王老板"), dwNpcID);
	}else if(strPropt.Find(_T("将伤药送给紫霞真人")) > 0){
		CallRunMap(_T("揽仙镇外"),  _T("117"), _T("155"));
	}else if(strPropt.Find(_T("赶紧回揽仙镇")) > 0){
		CallRunMap(_T("揽仙镇"),  _T("223"), _T("135"));
	}else if(strPropt.Find(_T("找卜老板道别")) > 0){
		TaskCallNpc(_T("卜老板"), dwNpcID);
	}else if(strPropt.Find(_T("把装备佩戴上")) > 0){
		//穿装备, 位置：0x1武器、0x2头、0x3衣服、0x33腰带、鞋子0xA
		TaskLoadEquip(0x2);
		TaskLoadEquip(0x3);
		TaskLoadEquip(0xA);
		TaskLoadEquip(0x33);
	}else if(strPropt.Find(_T("向张老板道别")) >0){
		TaskCallNpc(_T("张老板"), dwNpcID);
	}else if(strPropt.Find(_T("把武器佩戴上")) > 0){
		//穿装备, 位置：0x1武器、0x2头、0x3衣服、0x33腰带、鞋子0xA
		TaskLoadEquip(0x1);
	}else if(strPropt.Find(_T("向王老板道别")) > 0){
		TaskCallNpc(_T("王老板"), dwNpcID);
	}else if(strPropt.Find(_T("前往揽仙镇(208,145)")) > 0){
		CallRunMap(_T("揽仙镇"),  _T("208"), _T("145"));
	}else if(strPropt.Find(_T("到门派指引人处")) > 0){
		if(TaskCallNpc(_T("门派指引人"), dwNpcID)){
			char *szTxt = CallGetTalkCtrlItemTxtByIndex(1);
			CallClickTalkCtrlItem(dwNpcID, szTxt);
		}
	}else{
		if(strcmp(RoleInfo.szMapName, "终南山") == NULL){
			if(strAlias.Find(_T("稍作准备")) > 0){
				CallRunMap(_T("终南山"),  _T("36"), _T("12"));
			}else{
				WRITE_LOG("1~9级新手任务 完结 ...");
			}
		}else if(strcmp(RoleInfo.szMapName, "五龙山") == NULL){
			if(strAlias.Find(_T("稍作准备")) > 0){
				CallRunMap(_T("五龙山"),  _T("36"), _T("12"));
			}else{
				WRITE_LOG("1~9级新手任务 完结 ...");
			}
		} else if(strcmp(RoleInfo.szMapName, "凤凰山") == NULL){
			if(strAlias.Find(_T("稍作准备")) > 0){
				CallRunMap(_T("凤凰山"),  _T("36"), _T("12"));
			}else{
				WRITE_LOG("1~9级新手任务 完结 ...");
			}
		} else if(strcmp(RoleInfo.szMapName, "乾元山") == NULL){
			if(strAlias.Find(_T("稍作准备")) > 0){
				CallRunMap(_T("乾元山"),  _T("36"), _T("12"));
			}else{
				WRITE_LOG("1~9级新手任务 完结 ...");
			}
		}else if(strcmp(RoleInfo.szMapName, "骷髅山") == NULL){
			if(strAlias.Find(_T("稍作准备")) > 0){
				CallRunMap(_T("骷髅山"),  _T("36"), _T("12"));
			}else{
				WRITE_LOG("1~9级新手任务 完结 ...");
			}
		}else{
			WRITE_LOG("1~9级新手任务 完结 ...");
		}
	}
}

void Do_10_19_Task(STaskInfo NeedDo)
{
	CString strPropt = NeedDo.strPrompt;
	CString strAlias = NeedDo.strAlias;
	strPropt = TrimUnUse(strPropt);
	//
	CString strTongZhi;
	if(strcmp(RoleInfo.szMapName, "云霄洞") == NULL){
		strTongZhi = _T("云霄童子");
	}else if(strcmp(RoleInfo.szMapName, "玉柱洞") == NULL){
		strTongZhi = _T("碧玉童子");
	}else if(strcmp(RoleInfo.szMapName, "金光洞") == NULL){
		strTongZhi = _T("赤霞童子");
	}else if(strcmp(RoleInfo.szMapName, "斗阙宫") == NULL){
		strTongZhi = _T("水灵童子");
	}else if(strcmp(RoleInfo.szMapName, "白骨洞") == NULL){
		strTongZhi = _T("彩云童子");
	}else{
		strTongZhi = _T("noghing");
	}
	//
	DWORD dwOutNpcID = -1;
	if(strAlias.Find(_T("学习技能")) > 0){
		if(TaskCallNpc(strTongZhi, dwOutNpcID)){
			DWORD dwSkillID = -1;
			if (strTongZhi == _T("云霄童子")){
				dwSkillID = 0xb;
			}else if(strTongZhi == _T("碧玉童子")){
				dwSkillID = 0x3D;
			}else if(strTongZhi == _T("水灵童子")){
				dwSkillID = 0x6E;
			}else if(strTongZhi == _T("赤霞童子")){
				dwSkillID = 0xA1;
			}else if(strTongZhi == _T("彩云童子")){
				dwSkillID = 0xD2;
			}
			if (dwSkillID >0){
				CallLeanSkill(dwSkillID, dwOutNpcID, 1);
			}
		}
	}else if(strAlias.Find(_T("师门教诲")) > 0){
		if(TaskCallNpc(strTongZhi, dwOutNpcID)){
			char *szTxt1 = CallGetTalkCtrlItemTxtByIndex(1);
			CallClickTalkCtrlItem(dwOutNpcID, szTxt1);
		}
	}else if(strAlias.Find(_T("师傅送宠")) > 0){
		if(TaskCallNpc(strTongZhi, dwOutNpcID)){
			char *szTxt1 = CallGetTalkCtrlItemTxtByIndex(1);
			CallClickTalkCtrlItem(dwOutNpcID, szTxt1);
		}
	}else if(strAlias.Find(_T("求师傅指点")) > 0){
		if(TaskCallNpc(strTongZhi, dwOutNpcID)){
			char *szTxt1 = CallGetTalkCtrlItemTxtByIndex(1);
			CallClickTalkCtrlItem(dwOutNpcID, szTxt1);
		}
	}
}


void LoopTask()
{
	DWORD dwIsCatonState = CallGetCatonState();
	char *szCtrlName = CallGetCurrentCtrlName();
	if(1 == dwIsCatonState){
		//剧情状态
		if(strcmp(szCtrlName, "DramaDlg") == NULL){
			DWORD dwCtrlAddr = CallGetCtrlAddr("DramaDlg", "EscBtn");
			if(dwCtrlAddr != -1)
			{
				CallClickCtrl(dwCtrlAddr);
			}
		}
	}
	//问道小子处理
	AskTaoXiaoZhi();
	//WRITE_LOG("flag:%d, flag:%d", s_dwAuto_1_9, s_dwAuto_10_19);
	//
	if(1 == s_dwAuto_1_9){
		std::vector<STaskInfo> TaskInfos;
		CallGetAllTaskInfo(TaskInfos);
		for(unsigned int  i = 0; i< TaskInfos.size(); i ++){
			if(TaskInfos[i].strType.Find(_T("1-9级新手任务")  > 0)){
				CallUpdateRoleInfo();
				// 1. 普通 2.老君 3. 战斗
				if(1== RoleInfo.dwState && 0==RoleInfo.dwRunPath && 0== dwIsCatonState){
					if(strcmp(szCtrlName, "DramaDlg") != NULL){
						//WRITE_LOG("1~9级新手任务 ...");
						Do_1_9_Task(TaskInfos[i]);
					}
				}
			}
		}
	}else if(1 == s_dwAuto_10_19){
		std::vector<STaskInfo> TaskInfos;
		CallGetAllTaskInfo(TaskInfos);
		for(unsigned int  i = 0; i< TaskInfos.size(); i ++){
			if(TaskInfos[i].strType.Find(_T("10-19级新手任务")  > 0)){
				CallUpdateRoleInfo();
				// 1. 普通 2.老君 3. 战斗
				if(1== RoleInfo.dwState && 0==RoleInfo.dwRunPath && 0== dwIsCatonState){
					if(strcmp(szCtrlName, "DramaDlg") != NULL){
						//WRITE_LOG("1~9级新手任务 ...");
						Do_10_19_Task(TaskInfos[i]);
					}
				}
			}
		}
	}
}
