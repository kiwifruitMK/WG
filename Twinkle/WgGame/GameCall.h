#ifndef _H_GAME_CALL_H
#define  _H_GAME_CALL_H

extern char g_szAccount[];
extern char g_szPasswd[];
extern char g_szClickLogin[];
extern char g_szLines[];
extern char g_szOklLine[];
extern char g_szSelRole[];
extern char g_szCheckOk[]; 

typedef enum
{
	E_LOGIN_INVAILD= 0, //无效状态
	E_LOGIN_CHECK = 2, //登录验证状态界面
	E_LOGIN_INPUT = 3,   //可以输入密码界面
	E_LOGIN_LINE	= 6,    //选择分线界面
	E_LONG_ROLES = 8,	 //选角色界面
} ELoginUI;

//任务结构
typedef struct __STaskInfo
{
	CString strType;
	CString strPrompt;
	CString strTips;
	CString strAlias;
}STaskInfo, *pSTaskInfo;


//人物属性信息
typedef struct __SRoleInfo
{
	DWORD dwMaxHP;
	DWORD dwHP;
	DWORD dwMaxMP;
	DWORD dwMP;
	//
	DWORD dwLevel;
	DWORD dwX;
	DWORD dwY;
	char szName[MAX_PATH];  
	char szMapName[MAX_PATH];   //当前地图名字
	DWORD dwState;  //人物状态
	DWORD dwRunPath; //寻路状态
	DWORD dwCash; 
	DWORD dwVoucher;
	DWORD dwRemainPoint;
	DWORD dwPolar; //职业 1， 2， 3， 4， 5  金、木、水、火、土
}SRoleInfo, *pSRoleInfo;

typedef struct __SNpcInfo
{
	char szNpcName[MAX_PATH]; 
	char szLMapName[MAX_PATH];
	char szSMapName[MAX_PATH];
	DWORD dwNpcAddr;
	//DWORD dwNpcID;   //暂缺
	DWORD dwX;
	DWORD dwY;
}SNpcInfo, *pSNpcInfo;


typedef struct __SSkill
{
	char szSkillName[MAX_PATH];
	DWORD dwSkillLV;
	DWORD dwManaCost;
}SSkill;

extern SRoleInfo RoleInfo;

void LogRecord(TCHAR*  szText);
bool CallTypeAccountPasswd(DWORD uCharacter, DWORD uAddress);  //输入账号密码
bool CallClickLogin(DWORD dwAddress);   //点击登录按钮
bool CallSelLine(DWORD dwAddress); //选择线路
bool CallOkLine(DWORD dwAddress); //选择分线后的确认键
bool CallSelRoleAndLogin(DWORD dwAddress, DWORD dwIndexRole);  //选择角色并且进入游戏
ELoginUI CallGetLoginUI();  //登录界面状态
bool CallIsEnterGame(); //是否登录到游戏
bool CallCheckOk(DWORD dwAddress); //点击登陆验证按钮进入输入账号密码界面
bool CallRunMap(CString strMapName, CString strMapX, CString strMapY); //跨图寻路call
bool CallRunNPC(CString strNPCName); //NPC 寻路call
bool CallGetAllTaskInfo(std::vector<STaskInfo> &TaskInfos); //获取任务信息
void CallUpdateRoleInfo(); //更新人物信息
char* CallGetCurrentCtrlName(); //获取当前控件信息
DWORD CallGetCatonState(); //获取当前是否在剧情动画界面， 0：普通界面  1：动画界面
DWORD CallGetCtrlAddr(char* szCtrlName, DWORD dwTopWnd); //获取控件指针
DWORD CallGetCtrlAddr(char* szParent, char* szChild); //获取控件指针
bool CallClickCtrl(DWORD dwCtrlAddr); //点击控件
DWORD CallGetRoundActor(char* szActorName); //查找周围Actor
DWORD CallGetRoundActorID(DWORD dwActorAddr); //根据Actor地址，获取ActorID
DWORD CallGetRoundActorID(DWORD dwX, DWORD dwY); //根据Actor的x,y，获取ActorID
bool CallClickNpc(DWORD dwNpcID);   //点击对话Npc
DWORD CallGetNpcTalkCtrlAddr();  //获取Npc对话框控件指针 TalkMenuDlg
DWORD CallGetTalkCtrlNpcID(); //获取Npc对话控件窗口的NpcID
char*  CallGetTalkCtrlItemTxtByIndex(DWORD dwIndex); //获取当前对话框第index个项文本
bool CallClickTalkCtrlItem(DWORD dwNpcID, char* szTxt); // 点击Ncp对话的某一项
bool CallGetNpcInfo(char *szNpcName, SNpcInfo &NpcInfo);// 获取一个Npc的信息
bool CallGetNpcInfo(CString strNpcName, SNpcInfo &NpcInfo);//获取一个Npc的信息
void CallGetRoundMonster(); //遍历周围怪物信息
bool CallFindGoods(char* szNeedFind, DWORD &dwNum, DWORD &dwPos);  //根据物品名字找物品指针
bool CallLoadEquip (DWORD dwPos, DWORD dwPart); //穿装备, 位置：0x1武器、0x2头、0x3衣服、0x33腰带、鞋子0xA
DWORD CallRoleLevel();  //获取角色等级
bool CallAddPoint(DWORD dwDex,  DWORD dwStr, DWORD dwWiz, DWORD dwCon); //人物加点
bool CallLeanSkill(DWORD dwSkillID, DWORD dwNPCID, DWORD dwUpLV); //技能升级
bool CallGetAllSkill(std::vector<SSkill> & skills);// 遍历技能
#endif