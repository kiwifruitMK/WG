// Other.cpp : 实现文件
//

#include "stdafx.h"
#include "WgGame.h"
#include "Other.h"
#include "afxdialogex.h"


// COther 对话框

IMPLEMENT_DYNAMIC(COther, CDialog)

COther::COther(CWnd* pParent /*=NULL*/)
	: CDialog(COther::IDD, pParent)
{
	m_NPCName = _T("");
	m_MapName = _T("");
	m_MapX = _T("");
	m_MapY = _T("");
}

COther::~COther()
{
}

void COther::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_NPC_NAME, m_NPCName);
	DDX_Text(pDX, IDC_MAPNAME, m_MapName);
	DDX_Text(pDX, IDC_MAPX, m_MapX);
	DDX_Text(pDX, IDC_MAPY, m_MapY);
}


BEGIN_MESSAGE_MAP(COther, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, &COther::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &COther::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON4, &COther::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON3, &COther::OnBnClickedButton3)
END_MESSAGE_MAP()


// COther 消息处理程序

void COther::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	std::vector<STaskInfo> TaskInfos;
	CallGetAllTaskInfo(TaskInfos);
	for(unsigned int  i = 0; i< TaskInfos.size(); i ++){
		MessageBox(TaskInfos[i].strType, _T("type"));
		MessageBox(TaskInfos[i].strPrompt, _T("prop"));
		MessageBox(TaskInfos[i].strTips, _T("tip"));
		MessageBox(TaskInfos[i].strAlias, _T("alias"));
	}
}

void COther::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	if(!m_NPCName.IsEmpty()){
		CallRunNPC(m_NPCName);
	}
}

void COther::OnBnClickedButton4()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	if(!m_MapName.IsEmpty() && !m_MapX.IsEmpty() && !m_MapY.IsEmpty()){
		CallRunMap(m_MapName, m_MapX, m_MapY);
	}
}

void COther::OnBnClickedButton3()
{
	// TODO: 在此添加控件通知处理程序代码
	std::vector<SSkill> skills;
	if(CallGetAllSkill(skills)){
		for(unsigned i = 0; i< skills.size(); i++)
		{
			WRITE_LOG("name:%s, lv:%d, cost:%d", skills[i].szSkillName, skills[i].dwSkillLV, skills[i].dwManaCost);
		}
	}
	CallGetRoundMonster();

}
