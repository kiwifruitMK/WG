// Account.cpp : 实现文件
//

#include "stdafx.h"
#include "WgGame.h"
#include "Account.h"
#include "afxdialogex.h"
#include "AutoTask.h"

#define TIMER_1  1001 //用于自动登录游戏
#define TIME_DELAY_1 3000 //3秒钟延时


// CAccount 对话框

IMPLEMENT_DYNAMIC(CAccount, CDialog)

CAccount::CAccount(CWnd* pParent /*=NULL*/)
	: CDialog(CAccount::IDD, pParent)
	, m_Account(_T(""))
	, m_Passwd(_T(""))
	, m_index(_T(""))
	, m_Line(_T(""))
{

}

CAccount::~CAccount()
{
}

void CAccount::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Check(pDX, IDC_CHECK1, m_IsAutoLogin);
	DDX_Text(pDX, IDC_ACCOUNT, m_Account);
	DDX_Text(pDX, IDC_PASSWD, m_Passwd);
	DDX_Text(pDX, IDC_INDEX, m_index);
	DDX_Text(pDX, IDC_LINE, m_Line);
}


BEGIN_MESSAGE_MAP(CAccount, CDialog)
	ON_BN_CLICKED(IDC_CHECK1, &CAccount::OnBnClickedCheck1)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTON1, &CAccount::OnBnClickedButton1)
END_MESSAGE_MAP()


// CAccount 消息处理程序

//输入账号密码
void TypeNeedType(CString &strAccount, CString &strPasswd)
{
	CString strLogAccount;
	CString strLogPasswd;
	strLogAccount = strLogAccount + _T("你输入的账号：") + strAccount;
	strLogPasswd = strLogPasswd + _T("你输入的密码:") + strPasswd;
	pMainDlg->LogRecord(strLogAccount);
	pMainDlg->LogRecord(strLogPasswd);
	//
	std::vector<DWORD> AccountAddress;
	std::vector<DWORD> PasswdAddress;
	AccountAddress = ScanAddress(g_szAccount);
	PasswdAddress = ScanAddress(g_szPasswd);
	char *szAccount = strAccount.GetBuffer();
	char *szPasswd = strPasswd.GetBuffer();
	//
	CString strLog;
	for(size_t i = 0; i<AccountAddress.size(); i++){
		strLog.Format(_T("账号地址:%d:%0X"), i + 1, AccountAddress[i]);
		pMainDlg->LogRecord(strLog);
		//
		if(strcmp((char*)AccountAddress[i] + 0x94, "Edit") == 0){
			pMainDlg->LogRecord(_T("找到账号"));
			//
			char *szTmp = szAccount;
			CallTypeAccountPasswd((DWORD)szTmp[0], AccountAddress[i]);  //选中
			while(*szTmp++ != '\0'){
				CallTypeAccountPasswd((DWORD)(*szTmp), AccountAddress[i]);
			}
		} 
	}
	for(size_t i= 0; i<PasswdAddress.size(); i++){
		strLog.Format(_T("密码地址:%d:%0X"), i + 1, PasswdAddress[i]);
		pMainDlg->LogRecord(strLog);
		char *szTmp = szPasswd;
		CallTypeAccountPasswd((DWORD)szTmp[0],  PasswdAddress[i]);  //选中
		while(*szTmp++ != '\0'){
			CallTypeAccountPasswd((DWORD)(*szTmp), PasswdAddress[i]);
		}
	}
}
//点击登录按钮
void RoleLogin()
{
	std::vector<DWORD> Buttons;
	Buttons = ScanAddress(g_szClickLogin);
	for(size_t i = 0; i < Buttons.size(); i ++){
		if(strcmp((char*)Buttons[i] + 0x94, "EnterBtn") == 0){
			CString strLog;
			strLog.Format(_T("登录按钮地址:%X,"),  Buttons[i]);
			pMainDlg ->LogRecord(strLog);
			 CallClickLogin(Buttons[i]);
		}
	}
}
//选择线路, 如果需要选择九线直接写用大写的九
void SelLine(CString strLine)   //
{
		std::vector<DWORD> Lines;
		Lines = ScanAddress(g_szLines);
		DWORD dwFindAddress = 0;
		DWORD dwLastLength = 1000;
		for(size_t i = 0; i< Lines.size(); i++){
			//wchar_t *wcLine = AnsiToUnicode((char*)Lines[i] + 0x94);
			CString strLineName = (char*)Lines[i] + 0x94;
			if(strLineName.Find(strLine) >=0 ){
				if((DWORD)strLineName.GetLength() < dwLastLength){
					dwFindAddress = Lines[i];
					dwLastLength = strLineName.GetLength();
				}
			}
			//delete [] wcLine;
		}
		CString strLog;
		strLog.Format(_T("线路对象地址:%X"), dwFindAddress);
		pMainDlg ->LogRecord(strLog);
		if(0 != dwFindAddress){
			CallSelLine(dwFindAddress);
		}else
		{				
				pMainDlg ->LogRecord(_T("没有找到指定的分线~"));
		}
}
//选择线路后OK
void OkLine()
{
	std::vector<DWORD> Buttons;
	Buttons = ScanAddress(g_szOklLine);
	CString strLog;
	for(size_t i = 0; i< Buttons.size(); i++){
		
			if(strcmp((char*)Buttons[i] + 0x94, "EnterBtn") == 0){
				strLog.Format(_T("线路进入地址:%X,"), Buttons[i]);
				pMainDlg ->LogRecord(strLog);
				CallOkLine(Buttons[i]);
			}
			
	}
}
//选择第几个角色进入
void SelRoleAndLogin(DWORD dwIndexRole)
{
	std::vector<DWORD> Roles;
	Roles = ScanAddress(g_szSelRole);
	CString strLog;
	for(size_t i = 0; i < Roles.size(); i++){
		strLog.Format(_T("角色地址：%X"), Roles[i]);
		pMainDlg->LogRecord(strLog);
		CallSelRoleAndLogin(Roles[i] + 0x300, dwIndexRole);
	}
}
//获取登录界面状态
ELoginUI GetLoginUI() 
{
	ELoginUI LoginUIStatus = CallGetLoginUI();
	if (E_LONG_ROLES == LoginUIStatus){
		std::vector<DWORD> Buttons;
		Buttons = ScanAddress(g_szClickLogin);
		if(Buttons.size() == 3){
			LoginUIStatus = E_LOGIN_INVAILD;
		}
	}else{
		//nothing
	}
	return LoginUIStatus;
}

//点击登陆验证按钮进入输入账号密码界面
void CheckOk()
{
	std::vector<DWORD> Buttons;
	Buttons = ScanAddress(g_szCheckOk);
	CString strLog;
	for(size_t i = 0; i< Buttons.size(); i++){
		if(strcmp( (char*)Buttons[i] + 0x94, "CfmBtn") == 0){
			strLog.Format(_T("地址:%0X"), Buttons[i] );
			pMainDlg ->LogRecord(strLog);
			CallCheckOk(Buttons[i]);
		}
	}
}

void CAccount::OnBnClickedCheck1()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	if(m_IsAutoLogin){
		if (! m_Account.IsEmpty() 
			&& ! m_Passwd.IsEmpty() 
			&& ! m_index.IsEmpty()
			&& ! m_Line.IsEmpty())
		{
			SetTimer(TIMER_1, TIME_DELAY_1, NULL);
		}else{
			m_IsAutoLogin = FALSE;
			UpdateData(FALSE);
			MessageBox(_T("请填写账号密码等信息~"), _T("自动登录提示"), MB_OK);
		}
	}else{
		KillTimer(TIMER_1);
		m_IsAutoLogin = FALSE;
		UpdateData(FALSE);
	}
}

BOOL CAccount::OnInitDialog()
{
	CDialog::OnInitDialog();
	// TODO:  在此添加额外的初始化
	m_index = _T("1");
	m_Line = _T("8");
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}

void CAccount::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	switch(nIDEvent)
	{
	case TIMER_1:
		//自动登录的事情
		AutoLogin();
		break;
	default:
		break;
	}
	CDialog::OnTimer(nIDEvent);
}


// 自动登录主逻辑
void CAccount::AutoLogin(void)
{
	if(!CallIsEnterGame()){
		ELoginUI LoginUIStatus = GetLoginUI();
		switch(LoginUIStatus)
		{
		case E_LOGIN_CHECK:
			CheckOk();
			break;
		case E_LOGIN_INPUT:
			UpdateData(TRUE);
			TypeNeedType(m_Account, m_Passwd);
			RoleLogin();
			break;
		case E_LOGIN_LINE:
			UpdateData(TRUE);
			SelLine(ToHanzi(ToInt(m_Line)));
			OkLine();
			break;
		case E_LONG_ROLES:
			UpdateData(TRUE);
			SelRoleAndLogin(ToInt(m_index) - 1);
			break;
		default:
			break;
		}
	}
	if(m_IsAutoLogin){
		SetTimer(TIMER_1, TIME_DELAY_1, NULL);
	}
}

extern CWgGameApp theApp;
void CAccount::OnBnClickedButton1()
{
	
}
