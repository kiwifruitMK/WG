#ifndef _H_BASE_H_
#define _H_BASE_H_

#define  CLIENT_CLASS_NAME		_T("AskTao")
#define  WG_DLL_NAME	_T("WgGame.dll")

#define  CONFIG_DIR _T("Config")
#define  LAUNCH_CONFIG _T("Launch.xml")
#define  WG_DLL_CONFIG _T("WgGame.xml")

#endif