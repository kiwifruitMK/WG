
void Launcher()
{
	Sleep(500);
	__asm
	{
		push eax
			mov eax,  0x0415404
			call	eax
			pop  eax
	}
	Sleep(500);
	CString cstrServerName;
	GetPrivateProfileString(L"Server",  L"Name",  L"2012",  cstrServerName.GetBuffer(MAX_PATH), MAX_PATH,  GetConfigPath());
	//GetPrivateProfileInt(L"Server", L"Num", 1, GetConfigPath());;
	g_pLog->WriteLog(CLog::LL_INFORMATION, "查找到配置区服：%s",  UnicodeToAnsi(cstrServerName));

	CString cstrClassName("Button");
	std::vector<HWND> Hwnds = FindWindowByName(GetCurrentProcessId(),  cstrServerName, cstrClassName);
	std::vector<HWND>::iterator begin = Hwnds.begin();
	std::vector<HWND>::iterator end = Hwnds.end();
	g_pLog->WriteLog(CLog::LL_INFORMATION, "查到到满足条件区服：%d",  Hwnds.size());
	for(; begin != end; begin ++)
	{
		g_pLog->WriteLog(CLog::LL_INFORMATION, "句柄ID：%x",   (HWND)(*begin));
		TCHAR strName[MAX_PATH] = {0};
		::GetWindowText( (HWND)(*begin), strName, MAX_PATH);
		if (cstrServerName == strName){
			g_pLog->WriteLog(CLog::LL_INFORMATION, "准备进入区服:%s, 实际名字:%s", 
				UnicodeToAnsi(cstrServerName.GetBuffer(0)),  
				UnicodeToAnsi(strName)
				);
			HWND hServer =  (HWND)(*begin);
			__asm
			{
				mov	ecx,   hServer
					mov	edx,  0x004E25E4

					mov	eax,  0x0041106E
					call		eax

					mov	eax,  0x0040F12D
					call		eax

					mov	eax,  0x0408c05
					call     eax
			}
		}
	}
}
