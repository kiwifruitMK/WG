#pragma once
#include "PEStruct.h"
#include "afxcmn.h"


// CExport 对话框

class CExport : public CDialog
{
	DECLARE_DYNAMIC(CExport)

public:
	CExport(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CExport();

// 对话框数据
	enum { IDD = IDD_EXPORT };
public:
	PEStruct *m_pPeInfo;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CListCtrl m_funcList;
};
