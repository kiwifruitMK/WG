#ifndef _H_MY_UTIL_H_
#define  _H_MY_UTIL_H_
#include <vector>
namespace MyUtil
{
	class ProcessUtil 
	{
	public:
		enum ExeType
		{
			E_EXE,
			E_DLL
		} ;
		struct ModuleInfo
		{
			TCHAR lpstrPath[MAX_PATH];
			TCHAR lpstrFileName[MAX_PATH];
			DWORD dwImageBase;
			DWORD dwImageSize;
			ExeType eType;
		};

		struct ProcessInfo
		{
			DWORD dwProcessId;
			std::vector<ModuleInfo> modules;
		};
		static  const std::vector<ProcessInfo>* GetProcessesInfo();
		static bool SetPrivilege(PCSTR  lpstrName, bool bEnablePrivilege);
	private:
		static const int nMaxModules = 512;  //一个进程最多允许多少module
		static std::vector<ProcessInfo> processes;
		ProcessUtil(){};
	};

	void OutputDebugStringF(const char *fmt, ...);
	void Nothing(const char* fmt, ...);
#ifdef _DEBUG
#define  DbgPrintf OutputDebugStringF
#else
	#define  DbgPrintf Nothing
#endif
};
#endif