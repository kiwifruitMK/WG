#ifndef _H_PESTRUCT_H_
#define  _H_PESTRUCT_H_

#include <windows.h>
#include <stdio.h>
#include <vector>
#include <string.h>


class PEStruct
{
public:
	enum
	{
		E_BY_ORDINAL,
		E_BY_NAME
	};
	struct FunInfo
	{
		DWORD dwType; 
		char	 szFuncName[MAX_PATH];
		DWORD dwOrdinal;
		DWORD dwFuncRva;
	};
	struct ImportTableInfo
	{
			char	 szDllName[MAX_PATH];
			std::vector<FunInfo> vecImportFunc;
	};

public:
	PEStruct();
	~PEStruct();
	bool OpenFile(const char *szFilePath);
	long GetFileSize();
	bool AddSection(const char *szSectionName, int nRate);
private:
	bool PePrase();
	DWORD Rva2Foa(DWORD dwRva);
	void CleanData();
public:
	PIMAGE_DOS_HEADER m_pDosHeader;
	PIMAGE_NT_HEADERS m_pNTHeader;
	PIMAGE_FILE_HEADER m_pFileHeader;
	PIMAGE_OPTIONAL_HEADER m_pOptHeader;
	PIMAGE_EXPORT_DIRECTORY m_pExport;
	std::vector<PIMAGE_SECTION_HEADER> m_secHeaders;
	char  m_szFilePath[MAX_PATH];
	//
	DWORD m_dwExportRva;
	DWORD m_dwExportSize;
	std::vector<FunInfo> m_Export;
	DWORD m_dwImportRva;
	DWORD m_dwImportSize;
	std::vector<ImportTableInfo> m_Import;
private:
	char *m_pFileBuff;
	long m_nFileSize;
	FILE *m_fp;
	bool m_bIsSucc;
};

#endif

