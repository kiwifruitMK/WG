//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by KiwifruitShell.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_KIWIFRUITSHELL_DIALOG       102
#define IDR_MAINFRAME                   128
#define IDI_WIN32                       129
#define IDI_WINOWS                      129
#define IDD_PE_VIEW                     130
#define IDD_SECTION_VIEW                131
#define IDD_DIR_TABLE                   132
#define IDD_IMPORT                      133
#define IDD_EXPORT                      134
#define IDC_MODULE_LIST                 1003
#define IDC_PROCESS_LIST                1004
#define IDC_PE_VIEW                     1005
#define IDC_ADD_SHELL                   1006
#define IDC_EXIT                        1007
#define IDC_ENTRY_POING                 1008
#define IDC_IMAGE_BASE                  1009
#define IDC_SIZE_IMAGE                  1010
#define IDC_BASE_DATA                   1011
#define IDC_SECTION_ALI                 1012
#define IDC_FILE_ALI                    1013
#define IDC_TIME_STAP                   1017
#define IDC_SIZE_HEADERS                1018
#define IDC_CHAR                        1019
#define IDC_CHECK_SUM                   1020
#define IDC_SIZE_OPT                    1021
#define IDC_SUB_SYS                     1022
#define IDC_NUM_SECTION                 1023
#define IDC_PE_VIEW_OK                  1032
#define IDC_PE_VIEW_SECS                1033
#define IDC_DIR                         1034
#define IDC_LIST1                       1035
#define IDC_DIR_TABLE_OK                1036
#define IDC_TAB_RVA_EXPORT              1037
#define IDC_DIRT_TABLE_EXPORT_SIZE      1038
#define IDC_IMPORT_NAME                 1038
#define IDC_IMPORT_FUNC_NAME            1038
#define IDC_DIR_TABLE_IMPORT_RVA        1039
#define IDC_IMPORR_NAME                 1039
#define IDC_DIR_TABLE_IMPORT_SIZE       1040
#define IDC_EXPORT_FUNC_NAME            1040
#define IDC_EXPORT_VIEW                 1041
#define IDC_IMPORT_VIEW                 1042

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        135
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1042
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
