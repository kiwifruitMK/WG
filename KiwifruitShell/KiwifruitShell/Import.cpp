// Import.cpp : 实现文件
//

#include "stdafx.h"
#include "KiwifruitShell.h"
#include "Import.h"
#include "afxdialogex.h"


// CImport 对话框

IMPLEMENT_DYNAMIC(CImport, CDialogEx)

CImport::CImport(CWnd* pParent /*=NULL*/)
	: CDialogEx(CImport::IDD, pParent)
{

}

CImport::~CImport()
{
}

void CImport::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_IMPORR_NAME, m_importName);
	DDX_Control(pDX, IDC_IMPORT_NAME, m_importFuncName);
}


BEGIN_MESSAGE_MAP(CImport, CDialogEx)
	ON_NOTIFY(NM_CLICK, IDC_IMPORR_NAME, &CImport::OnClickImporrName)
END_MESSAGE_MAP()


// CImport 消息处理程序


BOOL CImport::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	LONG lStyle;
	lStyle = GetWindowLong(m_importFuncName.m_hWnd, GWL_STYLE);//获取当前窗口style
	lStyle &= ~LVS_TYPEMASK; //清除显示方式位
	lStyle |= LVS_REPORT; //设置style
	SetWindowLong(m_importFuncName.m_hWnd, GWL_STYLE, lStyle);//设置style
	DWORD dwStyle = m_importFuncName.GetExtendedStyle();
	dwStyle |= LVS_EX_FULLROWSELECT;//选中某行使整行高亮（只适用与report风格的listctrl）
	dwStyle |= LVS_EX_GRIDLINES;//网格线（只适用与report风格的listctrl）
	dwStyle |= LVS_EX_DOUBLEBUFFER; //双缓冲，防止闪烁
	//dwStyle |= LVS_EX_CHECKBOXES;//item前生成checkbox控件
	m_importFuncName.SetExtendedStyle(dwStyle); //设置扩展风格
	//
	lStyle = GetWindowLong(m_importName.m_hWnd, GWL_STYLE);//获取当前窗口style
	lStyle &= ~LVS_TYPEMASK; //清除显示方式位
	lStyle |= LVS_REPORT; //设置style
	SetWindowLong(m_importName.m_hWnd, GWL_STYLE, lStyle);//设置style
	dwStyle = m_importName.GetExtendedStyle();
	dwStyle |= LVS_EX_FULLROWSELECT;//选中某行使整行高亮（只适用与report风格的listctrl）
	dwStyle |= LVS_EX_GRIDLINES;//网格线（只适用与report风格的listctrl）
	dwStyle |= LVS_EX_DOUBLEBUFFER; //双缓冲，防止闪烁
	//dwStyle |= LVS_EX_CHECKBOXES;//item前生成checkbox控件
	m_importName.SetExtendedStyle(dwStyle); //设置扩展风格
	m_importName.InsertColumn( 0,  _T("模块名"),  LVCFMT_LEFT,  210 );//插入列
	m_importFuncName.InsertColumn( 0, _T("函数名/序号"),  LVCFMT_LEFT,  210);
	//
	for(size_t i = 0; i < m_pPeinfo->m_Import.size(); i++)
	{
		CString strDllName;
		strDllName.Format(_T("%s"), m_pPeinfo->m_Import[i].szDllName);
		int nRow = m_importName.GetItemCount();
		m_importName.InsertItem(nRow,  strDllName);
	}
	m_nSel = 0;
	FlashFuncName();
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}


void CImport::FlashFuncName(void)
{
	m_importFuncName.SetRedraw(FALSE); 
	m_importFuncName.DeleteAllItems();
	for(size_t i = 0; i< m_pPeinfo->m_Import.size(); i++)
	{
		if(i == m_nSel)
		{
			for(size_t j = 0; j< m_pPeinfo->m_Import[i].vecImportFunc.size(); j++)
			{
				CString strImportType;
				if(PEStruct::E_BY_ORDINAL == m_pPeinfo->m_Import[i].vecImportFunc[j].dwType)
				{
					strImportType.Format(_T("0x%X"), m_pPeinfo->m_Import[i].vecImportFunc[j].dwOrdinal);
				}
				else
				{
					strImportType.Format(_T("%s"), m_pPeinfo->m_Import[i].vecImportFunc[j].szFuncName);
				}
				int nRow = m_importFuncName.GetItemCount();
				m_importFuncName.InsertItem(nRow,  strImportType);
			}
			break;
		}
	}
	m_importFuncName.SetRedraw(TRUE); 
}


void CImport::OnClickImporrName(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	int nItem = pNMItemActivate->iItem;
	if(nItem != -1 && nItem != m_nSel)
	{
		m_nSel = nItem;
		FlashFuncName();
	}
	*pResult = 0;
}
