// Directory.cpp : 实现文件
//

#include "stdafx.h"
#include "KiwifruitShell.h"
#include "Directory.h"
#include "afxdialogex.h"
#include "Export.h"
#include "Import.h"


// CDirectory 对话框

IMPLEMENT_DYNAMIC(CDirectory, CDialog)

CDirectory::CDirectory(CWnd* pParent /*=NULL*/)
	: CDialog(CDirectory::IDD, pParent)
	, m_strExportRva(_T(""))
	, m_strExportSize(_T(""))
	, m_strImportRva(_T(""))
	, m_strImportSize(_T(""))
{

}

CDirectory::~CDirectory()
{
}

void CDirectory::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TAB_RVA_EXPORT, m_strExportRva);
	DDX_Text(pDX, IDC_DIRT_TABLE_EXPORT_SIZE, m_strExportSize);
	DDX_Text(pDX, IDC_DIR_TABLE_IMPORT_RVA, m_strImportRva);
	DDX_Text(pDX, IDC_DIR_TABLE_IMPORT_SIZE, m_strImportSize);
}


BEGIN_MESSAGE_MAP(CDirectory, CDialog)
	ON_BN_CLICKED(IDC_DIR_TABLE_OK, &CDirectory::OnBnClickedDirTableOk)
	ON_BN_CLICKED(IDC_EXPORT_VIEW, &CDirectory::OnBnClickedExportView)
	ON_BN_CLICKED(IDC_IMPORT_VIEW, &CDirectory::OnBnClickedImportView)
END_MESSAGE_MAP()


// CDirectory 消息处理程序


void CDirectory::OnBnClickedDirTableOk()
{
	// TODO: 在此添加控件通知处理程序代码
	EndDialog(0);
}


BOOL CDirectory::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	m_strExportRva.Format("0x%X", m_pPeInfo->m_dwExportRva);
	m_strExportSize.Format("0x%X", m_pPeInfo->m_dwExportSize);
	m_strImportRva.Format("0x%X", m_pPeInfo->m_dwImportRva);
	m_strImportSize.Format("0x%X", m_pPeInfo->m_dwImportSize);
	UpdateData(FALSE);

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}


void CDirectory::OnBnClickedExportView()
{
	// TODO: 在此添加控件通知处理程序代码
	CExport exportTable;
	exportTable.m_pPeInfo = m_pPeInfo;
	exportTable.DoModal();
}


void CDirectory::OnBnClickedImportView()
{
	// TODO: 在此添加控件通知处理程序代码
	CImport importTable;
	importTable.m_pPeinfo = m_pPeInfo;
	importTable.DoModal();
}
