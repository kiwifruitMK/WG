// PEView.cpp : 实现文件
//

#include "stdafx.h"
#include "KiwifruitShell.h"
#include "PEView.h"
#include "afxdialogex.h"
#include "SectionTable.h"
#include "Directory.h"


// CPEView 对话框

IMPLEMENT_DYNAMIC(CPEView, CDialog)

CPEView::CPEView(CWnd* pParent /*=NULL*/)
	: CDialog(CPEView::IDD, pParent)
	, m_strEntryPoint(_T(""))
	, m_strImageBase(_T(""))
	, m_strSizeOfImage(_T(""))
	, m_strBaseData(_T(""))
	, m_strSectionAli(_T(""))
	, m_strFileAli(_T(""))
	, m_strSubSys(_T(""))
	, m_strNumSection(_T(""))
	, m_strTimeStamp(_T(""))
	, m_strSizeHeader(_T(""))
	, m_strChar(_T(""))
	, m_strCheckSum(_T(""))
	, m_strSizeOptHeaders(_T(""))
{
		m_pPeInfo = NULL;
}

CPEView::~CPEView()
{
}

void CPEView::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_ENTRY_POING, m_strEntryPoint);
	DDX_Text(pDX, IDC_IMAGE_BASE, m_strImageBase);
	DDX_Text(pDX, IDC_SIZE_IMAGE, m_strSizeOfImage);
	DDX_Text(pDX, IDC_BASE_DATA, m_strBaseData);
	DDX_Text(pDX, IDC_SECTION_ALI, m_strSectionAli);
	DDX_Text(pDX, IDC_FILE_ALI, m_strFileAli);
	DDX_Text(pDX, IDC_SUB_SYS, m_strSubSys);
	DDX_Text(pDX, IDC_NUM_SECTION, m_strNumSection);
	DDX_Text(pDX, IDC_TIME_STAP, m_strTimeStamp);
	DDX_Text(pDX, IDC_SIZE_HEADERS, m_strSizeHeader);
	DDX_Text(pDX, IDC_CHAR, m_strChar);
	DDX_Text(pDX, IDC_CHECK_SUM, m_strCheckSum);
	DDX_Text(pDX, IDC_SIZE_OPT, m_strSizeOptHeaders);
}


BEGIN_MESSAGE_MAP(CPEView, CDialog)
	ON_BN_CLICKED(IDC_PE_VIEW_OK, &CPEView::OnBnClickedPeViewOk)
	ON_BN_CLICKED(IDC_PE_VIEW_SECS, &CPEView::OnBnClickedPeViewSecs)
	ON_BN_CLICKED(IDC_DIR, &CPEView::OnBnClickedDir)
END_MESSAGE_MAP()


// CPEView 消息处理程序


BOOL CPEView::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	m_strEntryPoint.Format(_T("0x%X"), m_pPeInfo->m_pOptHeader->AddressOfEntryPoint);
	m_strBaseData.Format(_T("0x%X"), m_pPeInfo->m_pOptHeader->BaseOfData);
	m_strChar.Format(_T("0x%X"), m_pPeInfo->m_pOptHeader->DllCharacteristics);
	m_strCheckSum.Format(_T("0x%X"), m_pPeInfo->m_pOptHeader->CheckSum);
	m_strFileAli.Format(_T("0x%X"), m_pPeInfo->m_pOptHeader->FileAlignment);
	m_strImageBase.Format(_T("0x%X"), m_pPeInfo->m_pOptHeader->ImageBase);
	m_strNumSection.Format(_T("0x%X"), m_pPeInfo->m_pFileHeader->NumberOfSections);
	m_strSectionAli.Format(_T("0x%X"), m_pPeInfo->m_pOptHeader->SectionAlignment);
	m_strSizeHeader.Format(_T("0x%X"), m_pPeInfo->m_pOptHeader->SizeOfHeaders);
	m_strSizeOfImage.Format(_T("0x%X"), m_pPeInfo->m_pOptHeader ->SizeOfImage);
	m_strSizeOptHeaders.Format(_T("0x%X"), m_pPeInfo->m_pFileHeader ->SizeOfOptionalHeader);
	m_strSubSys.Format(_T("0x%X"), m_pPeInfo->m_pOptHeader->Subsystem);
	m_strTimeStamp.Format(_T("0x%X"), m_pPeInfo->m_pFileHeader->TimeDateStamp);
	CString strDlgTxt;
	strDlgTxt.Format(_T("[PE查看] %s"), m_pPeInfo->m_szFilePath);
	SetWindowText(strDlgTxt);
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}


void CPEView::OnBnClickedPeViewOk()
{
	// TODO: 在此添加控件通知处理程序代码
	EndDialog(0);
}


void CPEView::OnBnClickedPeViewSecs()
{
	// TODO: 在此添加控件通知处理程序代码
	CSectionTable sectionTable;
	sectionTable.m_pPeInfo = m_pPeInfo;
	sectionTable.DoModal();
}


void CPEView::OnBnClickedDir()
{
	// TODO: 在此添加控件通知处理程序代码
	CDirectory direc;
	direc.m_pPeInfo = m_pPeInfo;
	direc.DoModal();

}
