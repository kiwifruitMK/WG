
// KiwifruitShellDlg.h : 头文件
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"
#include "MyUtil.h"
#include "PEStruct.h"
#include "PEView.h"


// CKiwifruitShellDlg 对话框
class CKiwifruitShellDlg : public CDialogEx
{
// 构造
public:
	CKiwifruitShellDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_KIWIFRUITSHELL_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:

private:
	bool m_bIsInit;
	int nLastSel;
	TCHAR m_lpstrOpenPEFilePath[MAX_PATH];
	const std::vector<MyUtil::ProcessUtil::ProcessInfo> *m_pProcesses;
	CPEView m_PEView;
public:
	afx_msg void OnBnClickedExit();
	CListCtrl m_processList;
	CImageList m_processImageList;
	CListCtrl m_moduleList;
	void FlashProcesses(void);
	afx_msg void OnClickProcessList(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg void OnBnClickedPeView();
};
