#pragma once
#include "afxcmn.h"
#include "PEStruct.h"


// CSectionTable 对话框

class CSectionTable : public CDialog
{
	DECLARE_DYNAMIC(CSectionTable)

public:
	CSectionTable(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CSectionTable();

// 对话框数据
	enum { IDD = IDD_SECTION_VIEW };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CListCtrl m_sectionTable;
	PEStruct *m_pPeInfo;
};
