
// KiwifruitShellDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "KiwifruitShell.h"
#include "KiwifruitShellDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

	// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	// 实现
protected:
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CKiwifruitShellDlg 对话框




CKiwifruitShellDlg::CKiwifruitShellDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CKiwifruitShellDlg::IDD, pParent)
	, m_bIsInit(false)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CKiwifruitShellDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROCESS_LIST, m_processList);
	DDX_Control(pDX, IDC_MODULE_LIST, m_moduleList);
}

BEGIN_MESSAGE_MAP(CKiwifruitShellDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_EXIT, &CKiwifruitShellDlg::OnBnClickedExit)
	ON_NOTIFY(NM_CLICK, IDC_PROCESS_LIST, &CKiwifruitShellDlg::OnClickProcessList)
	ON_BN_CLICKED(IDC_PE_VIEW, &CKiwifruitShellDlg::OnBnClickedPeView)
END_MESSAGE_MAP()


// CKiwifruitShellDlg 消息处理程序

BOOL CKiwifruitShellDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	LONG lStyle;
	lStyle = GetWindowLong(m_processList.m_hWnd, GWL_STYLE);//获取当前窗口style
	lStyle &= ~LVS_TYPEMASK; //清除显示方式位
	lStyle |= LVS_REPORT; //设置style
	SetWindowLong(m_processList.m_hWnd, GWL_STYLE, lStyle);//设置style

	DWORD dwStyle = m_processList.GetExtendedStyle();
	dwStyle |= LVS_EX_FULLROWSELECT;//选中某行使整行高亮（只适用与report风格的listctrl）
	dwStyle |= LVS_EX_GRIDLINES;//网格线（只适用与report风格的listctrl）
	dwStyle |= LVS_EX_DOUBLEBUFFER; //双缓冲，防止闪烁
	//dwStyle |= LVS_EX_CHECKBOXES;//item前生成checkbox控件
	m_processList.SetExtendedStyle(dwStyle); //设置扩展风格

	lStyle = GetWindowLong(m_moduleList.m_hWnd, GWL_STYLE);//获取当前窗口style
	lStyle &= ~LVS_TYPEMASK; //清除显示方式位
	lStyle |= LVS_REPORT; //设置style
	SetWindowLong(m_moduleList.m_hWnd, GWL_STYLE, lStyle);//设置style
	dwStyle = m_moduleList.GetExtendedStyle();
	dwStyle |= LVS_EX_FULLROWSELECT;//选中某行使整行高亮（只适用与report风格的listctrl）
	dwStyle |= LVS_EX_GRIDLINES;//网格线（只适用与report风格的listctrl）
	dwStyle |= LVS_EX_DOUBLEBUFFER; //双缓冲，防止闪烁
	//dwStyle |= LVS_EX_CHECKBOXES;//item前生成checkbox控件
	m_moduleList.SetExtendedStyle(dwStyle); //设置扩展风格
	m_processList.InsertColumn( 0,  _T("进程名"),  LVCFMT_LEFT,  100 );//插入列
	m_processList.InsertColumn( 1, _T("PID"),  LVCFMT_LEFT,  100);
	m_processList.InsertColumn( 2, _T("ImageBase"),  LVCFMT_LEFT,  100 );
	m_processList.InsertColumn( 3, _T("ImageSize"),  LVCFMT_LEFT,  120);
	m_moduleList.InsertColumn( 0,  _T("模块名"),  LVCFMT_LEFT,  100 );//插入列
	m_moduleList.InsertColumn( 1, _T("ImageBase"),  LVCFMT_LEFT,  150 );
	m_moduleList.InsertColumn( 2, _T("ImageSize"),  LVCFMT_LEFT,  170);

	m_processImageList.Create(16, 16, ILC_COLOR32, 0, 255);
	//绑定图标列表
	m_processList.SetImageList(&m_processImageList, LVSIL_SMALL);   

	//
	MyUtil::ProcessUtil::SetPrivilege(SE_DEBUG_NAME, true);
	nLastSel = 0;
	m_bIsInit = false;
	FlashProcesses();
	m_bIsInit = true;
	m_bIsInit;


	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CKiwifruitShellDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CKiwifruitShellDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CKiwifruitShellDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CKiwifruitShellDlg::OnBnClickedExit()
{
	// TODO: 在此添加控件通知处理程序代码
	EndDialog(0);
}

//刷新模块列表
void CKiwifruitShellDlg::FlashProcesses(void)
{
	if( !m_bIsInit) m_pProcesses = MyUtil::ProcessUtil::GetProcessesInfo();
	m_processList.SetRedraw(FALSE); 
	m_moduleList.SetRedraw(FALSE);
	m_moduleList.DeleteAllItems();

	for(size_t i = 0; i< m_pProcesses->size(); i++)
	{
		(*m_pProcesses)[i].dwProcessId;
		size_t  nModuleNum = (*m_pProcesses)[i].modules.size();
		if(nModuleNum > 0)
		{
			for(size_t j = 0; j< nModuleNum; j++)
			{
				if(0 == j && !m_bIsInit)
				{
					int nRow = m_processList.GetItemCount();
					HICON hProcessIcon;
					hProcessIcon=::ExtractIcon(NULL, (*m_pProcesses)[i].modules[j].lpstrPath, 0);
					if(!hProcessIcon)
					{
						hProcessIcon = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_WIN32));
					}
					int nImage=m_processImageList.Add(hProcessIcon);
					m_processList.InsertItem(nRow,"1",nImage);
					m_processList.SetItemText(nRow, 0,  (*m_pProcesses)[i].modules[j].lpstrFileName);
					CString strPId;
					strPId.Format("%d",  (*m_pProcesses)[i].dwProcessId);
					m_processList.SetItemText(nRow, 1,  strPId);
					CString strImageBase;
					strImageBase.Format("0x%X",   (*m_pProcesses)[i].modules[j].dwImageBase);
					m_processList.SetItemText(nRow, 2,  strImageBase);
					CString strImageSize;
					strImageSize.Format("0x%X",   (*m_pProcesses)[i].modules[j].dwImageSize);
					m_processList.SetItemText(nRow, 3,  strImageSize);
					//
				}
				else if (0 != j && i == nLastSel)
				{
					int nRow = m_moduleList.GetItemCount();
					m_moduleList.InsertItem(nRow, (*m_pProcesses)[i].modules[j].lpstrFileName);
					CString strImageBase;
					strImageBase.Format("0x%X",   (*m_pProcesses)[i].modules[j].dwImageBase);
					m_moduleList.SetItemText(nRow, 1,  strImageBase);
					CString strImageSize;
					strImageSize.Format("0x%X",   (*m_pProcesses)[i].modules[j].dwImageSize);
					m_moduleList.SetItemText(nRow, 2,  strImageSize);

				}
			}
		}
	}
	m_processList.SetRedraw(TRUE);
	m_moduleList.SetRedraw(TRUE);
}

void CKiwifruitShellDlg::OnClickProcessList(NMHDR *pNMHDR, LRESULT *pResult)
{
	LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	// TODO: 在此添加控件通知处理程序代码
	int nItem = pNMItemActivate->iItem;
	if (nItem != -1)
	{
		if(nItem != nLastSel)
		{
			nLastSel = nItem;
			FlashProcesses();
		}
	}
	*pResult = 0;
}

void CKiwifruitShellDlg::OnBnClickedPeView()
{
	// TODO: 在此添加控件通知处理程序代码
	OPENFILENAME	openFile;
	ZeroMemory(&openFile, sizeof(openFile));
	ZeroMemory(m_lpstrOpenPEFilePath, sizeof(m_lpstrOpenPEFilePath));
	openFile.lStructSize = sizeof(openFile);
	openFile.lpstrFilter = _T("执行文件(*.exe，*.dll)\0*.exe;*.dll\0所有文件(*.*)\0*.*\0\0");
	openFile.lpstrFile = m_lpstrOpenPEFilePath; 
	openFile.nMaxFile = MAX_PATH;
	openFile.nFilterIndex = 1; 
	openFile.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST; 
	if (!GetOpenFileName(&openFile)) 
	{
		DWORD dwErrCode = GetLastError();
		if(0 != dwErrCode)
		{
			CString strErrMsg;
			strErrMsg.Format(_T("打开文件失败，错误码:%d"), dwErrCode);
			AfxMessageBox(strErrMsg);
		}
		return;
	}
	PEStruct * pPeInfo = new PEStruct;
	if(!pPeInfo -> OpenFile(m_lpstrOpenPEFilePath))
	{
		AfxMessageBox(_T("载入可能不是PE文件！"));
		return;
	}
	m_PEView.m_pPeInfo = pPeInfo;
	m_PEView.DoModal();
	delete pPeInfo;
}


BOOL CAboutDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化

	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}
