// SectionTable.cpp : 实现文件
//

#include "stdafx.h"
#include "KiwifruitShell.h"
#include "SectionTable.h"
#include "afxdialogex.h"


// CSectionTable 对话框

IMPLEMENT_DYNAMIC(CSectionTable, CDialog)

CSectionTable::CSectionTable(CWnd* pParent /*=NULL*/)
	: CDialog(CSectionTable::IDD, pParent)
{

}

CSectionTable::~CSectionTable()
{
}

void CSectionTable::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_sectionTable);
}


BEGIN_MESSAGE_MAP(CSectionTable, CDialog)
END_MESSAGE_MAP()


// CSectionTable 消息处理程序


BOOL CSectionTable::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	LONG lStyle;
	lStyle = GetWindowLong(m_sectionTable.m_hWnd, GWL_STYLE);//获取当前窗口style
	lStyle &= ~LVS_TYPEMASK; //清除显示方式位
	lStyle |= LVS_REPORT; //设置style
	SetWindowLong(m_sectionTable.m_hWnd, GWL_STYLE, lStyle);//设置style
	DWORD dwStyle = m_sectionTable.GetExtendedStyle();
	dwStyle |= LVS_EX_FULLROWSELECT;//选中某行使整行高亮（只适用与report风格的listctrl）
	dwStyle |= LVS_EX_GRIDLINES;//网格线（只适用与report风格的listctrl）
	dwStyle |= LVS_EX_DOUBLEBUFFER; //双缓冲，防止闪烁
	//dwStyle |= LVS_EX_CHECKBOXES;//item前生成checkbox控件
	m_sectionTable.SetExtendedStyle(dwStyle); //设置扩展风格
	//
	m_sectionTable.InsertColumn( 0,  _T("名字"),  LVCFMT_LEFT,  80 );//插入列
	m_sectionTable.InsertColumn( 1, _T("RVA"),  LVCFMT_LEFT,  80);
	m_sectionTable.InsertColumn( 2, _T("VSize"),  LVCFMT_LEFT,  80);
	m_sectionTable.InsertColumn( 3, _T("FOA"),  LVCFMT_LEFT,  80);
	m_sectionTable.InsertColumn( 4, _T("FSize"),  LVCFMT_LEFT,  80);
	m_sectionTable.InsertColumn( 5, _T("Flags"),  LVCFMT_LEFT,  80);
	//
	std::vector<PIMAGE_SECTION_HEADER> secHeaders = m_pPeInfo->m_secHeaders;
	for(size_t i = 0; i< secHeaders.size(); i++)
	{
		int nRow = m_sectionTable.GetItemCount();
		char szBuff[10] = {0};
		memcpy(szBuff, &secHeaders[i]->Name[0], 8);
		m_sectionTable.InsertItem(nRow, szBuff);
		CString strVa;
		strVa.Format(_T("0x%X"),  secHeaders[i]->VirtualAddress);
		m_sectionTable.SetItemText(nRow, 1, strVa);
		CString strVaSize;
		strVaSize.Format(_T("0x%X"),  secHeaders[i]->Misc.VirtualSize);
		m_sectionTable.SetItemText(nRow, 2, strVaSize);
		CString strFa;
		strFa.Format(_T("0x%X"),  secHeaders[i]->PointerToRawData);
		m_sectionTable.SetItemText(nRow, 3, strFa);
		CString strFSize;
		strFSize.Format(_T("0x%X"),  secHeaders[i]->SizeOfRawData);
		m_sectionTable.SetItemText(nRow, 4, strFSize);
		CString strFlag;
		strFlag.Format(_T("0x%X"),  secHeaders[i]->Characteristics);
		m_sectionTable.SetItemText(nRow, 5, strFlag);
		
	}
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}
