#pragma once
#include "PEStruct.h"


// CDirectory 对话框

class CDirectory : public CDialog
{
	DECLARE_DYNAMIC(CDirectory)

public:
	CDirectory(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDirectory();

// 对话框数据
	enum { IDD = IDD_DIR_TABLE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
public:
	PEStruct *m_pPeInfo;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedDirTableOk();
	virtual BOOL OnInitDialog();
	CString m_strExportRva;
	CString m_strExportSize;
	CString m_strImportRva;
	CString m_strImportSize;
	afx_msg void OnBnClickedExportView();
	afx_msg void OnBnClickedImportView();
};
