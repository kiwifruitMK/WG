#pragma once
#include "afxcmn.h"
#include "PEStruct.h"


// CImport 对话框

class CImport : public CDialogEx
{
	DECLARE_DYNAMIC(CImport)

public:
	CImport(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CImport();

// 对话框数据
	enum { IDD = IDD_IMPORT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
public:
	PEStruct *m_pPeinfo;
private:
	int m_nSel;
	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CListCtrl m_importName;
	CListCtrl m_importFuncName;
	void FlashFuncName(void);
	afx_msg void OnClickImporrName(NMHDR *pNMHDR, LRESULT *pResult);
};
