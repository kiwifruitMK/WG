#pragma once
#include "PEStruct.h"

// CPEView 对话框

class CPEView : public CDialog
{
	DECLARE_DYNAMIC(CPEView)

public:
	CPEView(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CPEView();

// 对话框数据
	enum { IDD = IDD_PE_VIEW };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
public:
	PEStruct *m_pPeInfo;

	DECLARE_MESSAGE_MAP()
public:
	CString m_strEntryPoint;
	CString m_strImageBase;
	CString m_strSizeOfImage;
	CString m_strBaseData;
	CString m_strSectionAli;
	CString m_strFileAli;
	CString m_strSubSys;
	CString m_strNumSection;
	CString m_strTimeStamp;
	CString m_strSizeHeader;
	CString m_strChar;
	CString m_strCheckSum;
	CString m_strSizeOptHeaders;
	virtual BOOL OnInitDialog();
	afx_msg void OnBnClickedPeViewOk();
	afx_msg void OnBnClickedPeViewSecs();
	afx_msg void OnBnClickedDir();
};
