// Export.cpp : 实现文件
//

#include "stdafx.h"
#include "KiwifruitShell.h"
#include "Export.h"
#include "afxdialogex.h"


// CExport 对话框

IMPLEMENT_DYNAMIC(CExport, CDialog)

CExport::CExport(CWnd* pParent /*=NULL*/)
	: CDialog(CExport::IDD, pParent)
{

}

CExport::~CExport()
{
}

void CExport::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EXPORT_FUNC_NAME, m_funcList);
}


BEGIN_MESSAGE_MAP(CExport, CDialog)
END_MESSAGE_MAP()


// CExport 消息处理程序


BOOL CExport::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  在此添加额外的初始化
	LONG lStyle;
	lStyle = GetWindowLong(m_funcList.m_hWnd, GWL_STYLE);//获取当前窗口style
	lStyle &= ~LVS_TYPEMASK; //清除显示方式位
	lStyle |= LVS_REPORT; //设置style
	SetWindowLong(m_funcList.m_hWnd, GWL_STYLE, lStyle);//设置style

	DWORD dwStyle = m_funcList.GetExtendedStyle();
	dwStyle |= LVS_EX_FULLROWSELECT;//选中某行使整行高亮（只适用与report风格的listctrl）
	dwStyle |= LVS_EX_GRIDLINES;//网格线（只适用与report风格的listctrl）
	dwStyle |= LVS_EX_DOUBLEBUFFER; //双缓冲，防止闪烁
	//dwStyle |= LVS_EX_CHECKBOXES;//item前生成checkbox控件
	m_funcList.SetExtendedStyle(dwStyle); //设置扩展风格

	m_funcList.InsertColumn( 0,  _T("函数名/序号"),  LVCFMT_LEFT,  210 );//插入列
	m_funcList.InsertColumn( 1, _T("RVA"),  LVCFMT_LEFT,  300);
	for(size_t i = 0; i< m_pPeInfo->m_Export.size(); i++)
	{
		CString strExportType;
		if(PEStruct::E_BY_ORDINAL == m_pPeInfo->m_Export[i].dwType)
		{
			strExportType.Format(_T("0x%X"), m_pPeInfo->m_Export[i].dwOrdinal);
		}
		else
		{
			strExportType.Format(_T("%s"), m_pPeInfo->m_Export[i].szFuncName);
		}
		CString strExportRva;
		strExportRva.Format(_T("0x%X"), m_pPeInfo->m_Export[i].dwFuncRva);
		//
		int nRow = m_funcList.GetItemCount();
		m_funcList.InsertItem(nRow,  strExportType);
		m_funcList.SetItemText(nRow, 1,  strExportRva);
	}

	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	// 异常: OCX 属性页应返回 FALSE
}
