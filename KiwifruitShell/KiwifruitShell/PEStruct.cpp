#include "PEStruct.h"
#include "MyUtil.h"

PEStruct::PEStruct()
{
	m_bIsSucc = false;
	m_fp = NULL;
	m_pFileBuff = NULL;
	m_pDosHeader = NULL;
	m_pNTHeader = NULL;
	m_pFileHeader = NULL;
	m_pOptHeader = NULL;
	m_nFileSize = 0;
	m_dwExportRva = 0;
	m_dwExportSize = 0;
	m_dwImportRva = 0;
	m_dwImportSize = 0;
	m_secHeaders.clear();
}

bool PEStruct::OpenFile(const char* szFilePath)
{
		fopen_s(&m_fp, szFilePath, "rb+");
		m_nFileSize = GetFileSize();
		m_pFileBuff = (char*)malloc(m_nFileSize);
		ZeroMemory(m_pFileBuff, m_nFileSize);
		if(!PePrase()) goto CLEAN;
		strcpy_s(m_szFilePath, MAX_PATH - 1, szFilePath);
		m_bIsSucc = true;
		goto FINISH;
CLEAN:
		m_bIsSucc = false;
		if(NULL !=  m_fp) fclose(m_fp);
		if(NULL != m_pFileBuff) free(m_pFileBuff);
		m_fp = NULL;
		m_pFileBuff = NULL;
		m_pDosHeader = NULL;
		m_pNTHeader = NULL;
		m_pFileHeader = NULL;
		m_pOptHeader = NULL;
		m_nFileSize = 0;
		m_dwExportRva = 0;
		m_dwExportSize = 0;
		m_dwImportRva = 0;
		m_dwImportSize = 0;
		m_secHeaders.clear();
		return false;
FINISH:
		return true;
}

PEStruct::~PEStruct()
{
	m_bIsSucc = false;
	if(NULL !=  m_fp) fclose(m_fp);
	if(NULL != m_pFileBuff) free(m_pFileBuff);
	m_fp = NULL;
	m_pFileBuff = NULL;
	m_pDosHeader = NULL;
	m_pNTHeader = NULL;
	m_pFileHeader = NULL;
	m_pOptHeader = NULL;
	m_nFileSize = 0;
	m_nFileSize = 0;
	m_dwExportRva = 0;
	m_dwExportSize = 0;
	m_dwImportRva = 0;
	m_dwImportSize = 0;
	m_secHeaders.clear();
	m_pExport = NULL;
}

bool PEStruct::PePrase()
{
	m_secHeaders.clear();
	m_pExport = NULL;
	if(NULL == m_pFileBuff) goto CLEAN;
	fread(m_pFileBuff, m_nFileSize, 1, m_fp);
	m_pDosHeader = (PIMAGE_DOS_HEADER)m_pFileBuff;
	if( IMAGE_DOS_SIGNATURE != m_pDosHeader->e_magic) goto CLEAN;
	m_pNTHeader = (PIMAGE_NT_HEADERS)(m_pFileBuff + m_pDosHeader -> e_lfanew);
	if( IMAGE_NT_SIGNATURE != m_pNTHeader->Signature ) goto CLEAN;
	m_pFileHeader = &m_pNTHeader->FileHeader;
	m_pOptHeader = &m_pNTHeader->OptionalHeader;
	//
	PIMAGE_SECTION_HEADER pFirstSection = IMAGE_FIRST_SECTION(m_pNTHeader);
	for(int i = 0; i < m_pFileHeader->NumberOfSections; i++)
	{
		m_secHeaders.push_back(&pFirstSection[i]);
	}
	//导出表
	DWORD dwExportRva =m_pOptHeader->DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress;
	m_dwExportRva = dwExportRva;
	m_dwExportSize = m_pOptHeader->DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].Size;
	//
	DWORD dwExportFoa = Rva2Foa(dwExportRva);
	PIMAGE_EXPORT_DIRECTORY pExport = (PIMAGE_EXPORT_DIRECTORY)(m_pFileBuff + dwExportFoa);
	if(pExport->NumberOfFunctions != 0)
	{
		DWORD dwNameRva = pExport->Name;
		DWORD dwNameFoa = Rva2Foa(dwNameRva);
		MyUtil::DbgPrintf("name:%s\n", (char*)(m_pFileBuff + dwNameFoa));

		MyUtil::DbgPrintf("timestamp:0x%X\n", pExport->TimeDateStamp);
		MyUtil::DbgPrintf("number of functions:0x%X\n", pExport ->NumberOfFunctions);
		MyUtil::DbgPrintf("number of names:0x%X\n", pExport ->NumberOfNames);
		MyUtil::DbgPrintf("base:0x%X\n", pExport ->Base);
		DWORD dwAddressOfNamesRva  = pExport ->AddressOfNames;
		DWORD dwAddressOfNamesFoa = Rva2Foa(dwAddressOfNamesRva);
		DWORD *pNameAddr =(DWORD*)(m_pFileBuff + dwAddressOfNamesFoa);
		DWORD dwCount1 = 0;
		std::vector<char*> vecFuncName;
		while(*pNameAddr && dwCount1 < pExport ->NumberOfNames)
		{
			DWORD dwFunNamFoa =Rva2Foa(*pNameAddr);
			//MyUtil::DbgPrintf("func name:%s\n", (char*)(m_pFileBuff + dwFunNamFoa));
			vecFuncName.push_back(m_pFileBuff + dwFunNamFoa);
			pNameAddr ++;
			dwCount1 ++;
		}
		DWORD dwAddressOfFunctionsRva  = pExport ->AddressOfFunctions;
		DWORD dwAddressOfFunctionsFoa = Rva2Foa(dwAddressOfFunctionsRva);
		DWORD dwCount3 = 0;
		DWORD *pFunctionAddr =(DWORD*)(m_pFileBuff + dwAddressOfFunctionsFoa);
		std::vector<DWORD> vecFuncAddr;
		while(dwCount3 < pExport ->NumberOfFunctions)
		{
			//MyUtil::DbgPrintf("Addr index = %d => 0x%X\n", dwCount3, *pFunctionAddr);
			vecFuncAddr.push_back(*pFunctionAddr);
			pFunctionAddr ++;
			dwCount3 ++;
		}
		//
		DWORD dwAddressOfOrdinalsRva  = pExport ->AddressOfNameOrdinals;
		DWORD dwAddressOfOrdinalsFoa = Rva2Foa(dwAddressOfOrdinalsRva);
		DWORD dwCount2 = 0;
		WORD *pOrdinalsAddr =(WORD*)(m_pFileBuff + dwAddressOfOrdinalsFoa);
		while(dwCount2 < pExport ->NumberOfFunctions)
		{
			FunInfo exportFunInfo;
			memset(&exportFunInfo, 0, sizeof(exportFunInfo));
			//
			if (dwCount2 <vecFuncName.size())
			{
				exportFunInfo.dwType = E_BY_NAME;
				lstrcpy(exportFunInfo.szFuncName, vecFuncName[dwCount2]);
				exportFunInfo.dwFuncRva = vecFuncAddr[*pOrdinalsAddr];
				MyUtil::DbgPrintf(" export by name:funname:%s addr:0x%X", vecFuncName[dwCount2], vecFuncAddr[*pOrdinalsAddr]);
			}
			else
			{
				exportFunInfo.dwType = E_BY_ORDINAL;
				exportFunInfo.dwOrdinal = pExport ->Base + *pOrdinalsAddr;
				exportFunInfo.dwFuncRva = vecFuncAddr[*pOrdinalsAddr];
				MyUtil::DbgPrintf(" export by Ordinals:Ordinals:0x%X addr:0x%X", pExport ->Base + *pOrdinalsAddr, vecFuncAddr[*pOrdinalsAddr]);
			}
			m_Export.push_back(exportFunInfo);
			pOrdinalsAddr ++;
			dwCount2 ++;
		}
	}
	//导入表
	DWORD dwImportRva =m_pOptHeader->DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress;
	m_dwImportRva = dwImportRva;
	m_dwImportSize = m_pOptHeader->DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size;
	DWORD dwImportFoa = Rva2Foa(dwImportRva);
	PIMAGE_IMPORT_DESCRIPTOR pImport = (PIMAGE_IMPORT_DESCRIPTOR)(m_pFileBuff + dwImportFoa);
	while(pImport->Name != 0)
	{
		ImportTableInfo importTableInfo;
		DWORD dwDllNamFoa = Rva2Foa(pImport->Name);
		lstrcpy(importTableInfo.szDllName, (char*)(dwDllNamFoa + m_pFileBuff));
		DWORD dwOriginalFirstThunkFoa = Rva2Foa(pImport->OriginalFirstThunk);
		PIMAGE_THUNK_DATA pINT = (PIMAGE_THUNK_DATA)(m_pFileBuff + dwOriginalFirstThunkFoa);
		while(pINT->u1.AddressOfData)
		{
			FunInfo importFunInfo;
			memset(&importFunInfo, 0, sizeof(importFunInfo));
			//
			if(IMAGE_SNAP_BY_ORDINAL(pINT->u1.Ordinal))
			{
				importFunInfo.dwType = E_BY_ORDINAL;
				importFunInfo.dwOrdinal = IMAGE_ORDINAL(pINT->u1.Ordinal);
				MyUtil::DbgPrintf("imort by ordinal， ordinal：0x%X", IMAGE_ORDINAL(pINT->u1.Ordinal));
			}
			else
			{
				DWORD dwFunNamRva = pINT->u1.AddressOfData;
				DWORD dwFunNamFoa = Rva2Foa(dwFunNamRva);
				PIMAGE_IMPORT_BY_NAME pIportName = (PIMAGE_IMPORT_BY_NAME)(m_pFileBuff +dwFunNamFoa);
				MyUtil::DbgPrintf("import by name, name:%s", pIportName->Name);
				//
				importFunInfo.dwType = E_BY_NAME;
				lstrcpy(importFunInfo.szFuncName, (char*)pIportName->Name);
			}
			importTableInfo.vecImportFunc.push_back(importFunInfo);
			pINT ++;
		}
		m_Import.push_back(importTableInfo);
		pImport ++;
	}

	goto FINISH;
CLEAN:
	return false;
FINISH:
	return true;
}

DWORD PEStruct::Rva2Foa(DWORD dwRva)
{
	for(size_t i = 0; i < m_secHeaders.size(); i++)
	{
		DWORD dwStartVa = m_secHeaders[i] ->VirtualAddress;
		DWORD dwCount1, dwCount2;
		if(m_secHeaders[i] ->Misc.VirtualSize >= m_secHeaders[i] ->SizeOfRawData)
		{
			dwCount1 = m_secHeaders[i] ->Misc.VirtualSize / m_pOptHeader->SectionAlignment;
			dwCount2 = m_secHeaders[i] ->Misc.VirtualSize % m_pOptHeader->SectionAlignment? 1:0;
		}
		else
		{
			dwCount1 = m_secHeaders[i] ->SizeOfRawData / m_pOptHeader->SectionAlignment;
			dwCount2 = m_secHeaders[i] ->SizeOfRawData % m_pOptHeader->SectionAlignment? 1:0;
		}
		DWORD dwSize = (dwCount1 + dwCount2) * m_pOptHeader->SectionAlignment;
		if(dwRva>= dwStartVa && dwRva < dwStartVa + dwSize)
		{
			return m_secHeaders[i] ->PointerToRawData + dwRva - dwStartVa;
		}

	}
	return dwRva;
}

void  PEStruct::CleanData()
{
	m_bIsSucc = false;
	if(NULL !=  m_fp) fclose(m_fp);
	if(NULL != m_pFileBuff) free(m_pFileBuff);
	m_fp = NULL;
	m_pFileBuff = NULL;
	m_pDosHeader = NULL;
	m_pNTHeader = NULL;
	m_pFileHeader = NULL;
	m_pOptHeader = NULL;
	m_nFileSize = 0;
	m_nFileSize = 0;
	m_dwExportRva = 0;
	m_dwExportSize = 0;
	m_dwImportRva = 0;
	m_dwImportSize = 0;
	m_secHeaders.clear();
	m_pExport = NULL;
}

long  PEStruct::GetFileSize()
{
	if(NULL ==m_fp)
	{
		return 0;
	}
	{
		long nCurPos, nLength;
		nCurPos = ftell(m_fp);
		fseek(m_fp, 0L, SEEK_END);
		nLength = ftell(m_fp);
		fseek(m_fp, nCurPos, SEEK_SET);
		return nLength;
	}
}
bool PEStruct::AddSection(const char *szSectionName, int nRate)
{
	char *pNewFileBuff = NULL;
	if(nRate <= 0) goto CLEAN;
	if(!m_bIsSucc) goto CLEAN;
	PIMAGE_OPTIONAL_HEADER pOpHeader = m_pOptHeader;
	DWORD dwSectionAlignment = pOpHeader->SectionAlignment;
	DWORD dwAddSize = nRate * dwSectionAlignment;
	DWORD dwNewFileSize = m_nFileSize + dwAddSize;
	DWORD dwCnt1 = (pOpHeader->SizeOfImage + dwAddSize) / dwSectionAlignment;
	DWORD dwCnt2 = (pOpHeader->SizeOfImage + dwAddSize) % dwSectionAlignment? 1:0;
	DWORD dwNewImageSize = (dwCnt1 +  dwCnt2) * dwSectionAlignment;
	//
	pNewFileBuff = (char*)malloc(dwNewFileSize);
	if(NULL == pNewFileBuff) goto CLEAN;
	ZeroMemory(pNewFileBuff, dwNewFileSize);
	memcpy(pNewFileBuff, m_pFileBuff, sizeof(IMAGE_DOS_HEADER));
	PIMAGE_DOS_HEADER pDosHeader = (PIMAGE_DOS_HEADER)pNewFileBuff;
	pDosHeader->e_lfanew = sizeof(IMAGE_DOS_HEADER);
	memcpy(
		pNewFileBuff + pDosHeader->e_lfanew, 
		m_pFileBuff + m_pDosHeader->e_lfanew,
		m_pOptHeader->SizeOfHeaders - m_pDosHeader->e_lfanew
		);
	memcpy(
		pNewFileBuff + m_pOptHeader->SizeOfHeaders,
		m_pFileBuff + m_pOptHeader->SizeOfHeaders,
		m_nFileSize - m_pOptHeader->SizeOfHeaders
		);
	//
	DWORD dwOldFileSize = GetFileSize();
	DWORD dwOldSizeOfImage = m_pOptHeader->SizeOfImage;
	free(m_pFileBuff);
	m_pFileBuff = pNewFileBuff;
	if(!PePrase()) goto FINISH;
	m_pOptHeader->SizeOfImage = dwNewImageSize;
	m_pFileHeader ->NumberOfSections += 1;
	PIMAGE_SECTION_HEADER pFirstSection = IMAGE_FIRST_SECTION(m_pNTHeader);
	memcpy(&pFirstSection[m_pFileHeader ->NumberOfSections - 1], pFirstSection, sizeof(IMAGE_SECTION_HEADER));
	ZeroMemory(&pFirstSection[m_pFileHeader ->NumberOfSections - 1].Name[0], IMAGE_SIZEOF_SHORT_NAME);
	memcpy(pFirstSection[m_pFileHeader ->NumberOfSections - 1].Name, szSectionName, IMAGE_SIZEOF_SHORT_NAME);
	pFirstSection[m_pFileHeader ->NumberOfSections - 1].VirtualAddress = dwOldSizeOfImage;
	pFirstSection[m_pFileHeader ->NumberOfSections - 1].Misc.VirtualSize = dwAddSize;
	pFirstSection[m_pFileHeader ->NumberOfSections - 1].PointerToRawData = dwOldFileSize;	
	pFirstSection[m_pFileHeader ->NumberOfSections - 1].SizeOfRawData = dwAddSize;
	m_nFileSize = dwNewFileSize;
	m_secHeaders.push_back(&pFirstSection[m_pFileHeader ->NumberOfSections - 1]);
	m_bIsSucc = true;
	goto FINISH;
CLEAN:
		m_bIsSucc = false;
		if(NULL !=  m_fp) fclose(m_fp);
		if(NULL != m_pFileBuff) free(m_pFileBuff);
		m_fp = NULL;
		m_pFileBuff = NULL;
		m_pDosHeader = NULL;
		m_pNTHeader = NULL;
		m_pFileHeader = NULL;
		m_pOptHeader = NULL;
		m_nFileSize = 0;
		m_secHeaders.clear();
		m_pExport = NULL;
		if(NULL !=pNewFileBuff) free(pNewFileBuff);
		return false;
FINISH:
	return true;
}