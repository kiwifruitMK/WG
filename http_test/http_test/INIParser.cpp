#include "INIParser.h"

//remove all blank space
std::string &TrimString(std::string &str)
{
    std::string::size_type pos = 0;
    while(str.npos != (pos = str.find(" ")))
        str = str.replace(pos, pos+1, "");
    return str;
}

//read in INI file and parse it
int INIParser::ReadINI(std::string path)
{
    std::ifstream in_conf_file(path.c_str());
    if(!in_conf_file) return 0;
    std::string str_line = "";
    std::string str_root = "";
    std::vector<ININode> vec_ini;
    while(getline(in_conf_file, str_line))
    {
        std::string::size_type left_pos = 0;
        std::string::size_type right_pos = 0;
        std::string::size_type equal_div_pos = 0;
        std::string str_key = "";
        std::string str_value = "";
        if((str_line.npos != (left_pos = str_line.find("["))) && (str_line.npos != (right_pos = str_line.find("]"))))
        {
            //cout << str_line.substr(left_pos+1, right_pos-1) << endl;
            str_root = str_line.substr(left_pos+1, right_pos-1);
        }


        if(str_line.npos != (equal_div_pos = str_line.find("=")))
        {
           str_key = str_line.substr(0, equal_div_pos);
           str_value = str_line.substr(equal_div_pos+1, str_line.size()-1);
           str_key = TrimString(str_key);
           str_value = TrimString(str_value);
           //cout << str_key << "=" << str_value << endl;
        }

        if((!str_root.empty()) && (!str_key.empty()) && (!str_value.empty()))
        {
           ININode ini_node(str_root, str_key, str_value);
           vec_ini.push_back(ini_node);
           //cout << vec_ini.size() << endl;
        }
    }
    in_conf_file.close();
    in_conf_file.clear();

    //vector convert to map
    std::map<std::string, std::string> map_tmp;
    for(std::vector<ININode>::iterator itr = vec_ini.begin(); itr != vec_ini.end(); ++itr)
    {
        map_tmp.insert(std::pair<std::string, std::string>(itr->root, ""));
    }

    SubNode sn;
    for(std::map<std::string, std::string>::iterator itr = map_tmp.begin(); itr != map_tmp.end(); ++itr)
    {
       //cout << itr->first << endl;
       for(std::vector<ININode>::iterator sub_itr = vec_ini.begin(); sub_itr != vec_ini.end(); ++sub_itr)
       {
           if(sub_itr->root == itr->first)
           {
               //cout << sub_itr->key << "=" << sub_itr->value << endl;
               sn.InsertElement(sub_itr->key, sub_itr->value);
           }
       }
       map_ini.insert(std::pair<std::string, SubNode>(itr->first, sn));
    }
    return 1;
}

//get value by root and key
std::string INIParser::GetValue(std::string root, std::string key)
{
    std::map<std::string, SubNode>::iterator itr = map_ini.find(root);
    std::map<std::string, std::string>::iterator sub_itr = itr->second.sub_node.find(key);
    if(!(sub_itr->second).empty())
        return sub_itr->second;
    return "";
}

//write ini file
int INIParser::WriteINI(std::string path)
{
    std::ofstream out_conf_file(path.c_str());
    if(!out_conf_file)
        return -1;

    //cout << map_ini.size() << endl;
    for(std::map<std::string, SubNode>::iterator itr = map_ini.begin(); itr != map_ini.end(); ++itr)
    {
       //cout << itr->first << endl;
       out_conf_file << "[" << itr->first << "]" << std::endl;
       for(std::map<std::string, std::string>::iterator sub_itr = itr->second.sub_node.begin(); sub_itr != itr->second.sub_node.end(); ++sub_itr)
       {
           //cout << sub_itr->first << "=" << sub_itr->second << endl;
           out_conf_file << sub_itr->first << "=" << sub_itr->second << std::endl;
       }
    }

    out_conf_file.close();
    out_conf_file.clear();
    return 1;
}

//set value
std::vector<ININode>::size_type INIParser::SetValue(std::string root, std::string key, std::string value)
{
    std::map<std::string, SubNode>::iterator itr = map_ini.find(root);
    if(map_ini.end() != itr)
        itr->second.sub_node.insert(std::pair<std::string, std::string>(key, value));
    else
    {
        SubNode sn;
        sn.InsertElement(key, value);
        map_ini.insert(std::pair<std::string, SubNode>(root, sn));
    }
    return map_ini.size();
}
