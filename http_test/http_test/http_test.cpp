// http_test.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"
#include "IHttp.h"
#include <iostream>
#include<fstream>
#include <regex>
#include <vector>
#include <list>
#include <map>
#include "ThreadPool.h"
#include <direct.h>
#include "INIParser.h"

#define  MAX_PAGE  400


CRITICAL_SECTION vFirstHrefLock;
std::list <string> vFirstHref;

CRITICAL_SECTION vSecondHrefLock;
std::list <string> vSecondHref;
string  strPathBase;  //保存在

void add_first_href(string strHref)
{
	EnterCriticalSection(&vFirstHrefLock);  
	vFirstHref.push_back(strHref); 
	LeaveCriticalSection(&vFirstHrefLock);  
}
string get_first_href()
{
	string strRtn;
	EnterCriticalSection(&vFirstHrefLock);  
	if(!vFirstHref.empty())  
	{  
		strRtn = vFirstHref.front();  
		vFirstHref.pop_front();  
	}  
	LeaveCriticalSection(&vFirstHrefLock); 
	return strRtn;
}
int get_first_href_size()
{
	int iSize = -1;
	EnterCriticalSection(&vFirstHrefLock);  
	iSize = vFirstHref.size();
	LeaveCriticalSection(&vFirstHrefLock);  
	return iSize;
}

////


void add_second_href(string strHref)
{
	EnterCriticalSection(&vSecondHrefLock);  
	vSecondHref.push_back(strHref); 
	LeaveCriticalSection(&vSecondHrefLock);  
}
string get_second_href()
{
	string strRtn;
	EnterCriticalSection(&vSecondHrefLock);  
	if(!vSecondHref.empty())  
	{  
		strRtn = vSecondHref.front();  
		vSecondHref.pop_front();  
	}  
	LeaveCriticalSection(&vSecondHrefLock); 
	return strRtn;
}

int get_second_href_size()
{
	int iSize = -1;
	EnterCriticalSection(&vSecondHrefLock);  
	iSize = vSecondHref.size();
	LeaveCriticalSection(&vSecondHrefLock);  
	return iSize;
}


void read_first_href(string &strUrl)
{
	IHttpInterface* pHttp = CreateHttpInstance(); 
	string strBody = pHttp->Request(strUrl, IHttpInterface::get);
	const std::regex pattern("<a class=\"img\" href=.+?>");
	const std::sregex_token_iterator end;
	for (std::sregex_token_iterator i(strBody.begin(),strBody.end(), pattern); i != end; i++)
	{
		int iBegin = i->str().find("href");
		string tmp1 = i->str().substr(iBegin + 6);
		int iEnd =  tmp1.find("\"");
		string tmp2 = tmp1.substr(0, iEnd);
		//std::cout<< tmp2 << std::endl;
		add_first_href(tmp2);
	}
	pHttp ->FreeInstance();

}

void read_second_href(string &strUrl)
{
	IHttpInterface* pHttp = CreateHttpInstance(); 
	string strBody = pHttp->Request(strUrl, IHttpInterface::get);
	const std::regex pattern("<img src=.+?.jpg\"/>");
	const std::sregex_token_iterator end;
	for (std::sregex_token_iterator i(strBody.begin(),strBody.end(), pattern); i != end; i++)
	{
		int iBegin = i->str().find("src");
		string tmp1 = i->str().substr(iBegin + 5);
		int iEnd =  tmp1.find("jpg");
		string tmp2 = tmp1.substr(0, iEnd + 3);
		//std::cout << tmp2 << std::endl;
		add_second_href(tmp2);
	}
	pHttp ->FreeInstance();

}

void down_load(string &strUrl)
{
	IHttpInterface* pHttp = CreateHttpInstance();
	std::cout << "准备下载：" << strUrl << std::endl;
	int iBegin = strUrl.rfind("/");
	string strTmp = strUrl.substr(iBegin + 1);
	DWORD tm = ::GetTickCount(); 
	char buff[50];
	sprintf_s(buff, "%ld", tm);
	string strSavePath = strPathBase + buff + strTmp;
	std::cout << "保存到：" << strSavePath << std::endl;
	pHttp ->Download(strUrl, strSavePath);
	pHttp ->FreeInstance();

}


void ThreadFun1(void *p)
{
	string strUrl = *((string*)p);
	read_first_href(strUrl);
}

void ThreadFun2(void *p)
{
	while(true){
		string strUrl = get_first_href();
		if (strUrl.length() != 0) {
			//std::cout << "准备分析下载目录:" << strUrl << std::endl;
			read_second_href(strUrl);
		}else{
			;
		}
		Sleep(10);
	}
}

void ThreadFun3(void *p)
{
	while(true){
		string strUrl = get_second_href();
		if (strUrl.length() != 0) {
			down_load(strUrl);
		}else{
			;
		}
		Sleep(10);
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	INIParser ini_parser;
	ini_parser.ReadINI("conf.ini");
	strPathBase = ini_parser.GetValue("default", "save_path");
	ini_parser.Clear();
	std::cout << "准备创建保存目录:" << strPathBase << std::endl;
	_mkdir(strPathBase.c_str() ) ;
	//
	ThreadPool tp1;
	InitializeCriticalSection(&vFirstHrefLock);
	InitializeCriticalSection(&vSecondHrefLock);
	std::vector<string> URLs;
	string strBase = "http://sexy.faceks.com/?page=";
	for(int i = 1; i <= MAX_PAGE ; i += 5){
		char buff[MAX_PATH];
		_itoa_s(i, buff, 10);
		//std::cout << strBase + buff << std::endl;
		URLs.push_back(strBase + buff );
	}
	for(std::vector<string>::iterator i = URLs.begin(); i != URLs.end(); i ++){
		
		tp1.Call(ThreadFun1, (void*)&(*i));
	}
	//
	ThreadPool tp2;
	for(int i= 1; i<= 4; i++){
		tp2.Call(ThreadFun2);
	}
	//
	ThreadPool tp3(8);
	for(int i= 1; i<= 8; i++){
		tp3.Call(ThreadFun3);
	}
	while(true){
		std::cout << "等待分析分析网页数量:" << get_first_href_size() << std::endl;
		std::cout << "等待分析下载图片数量:" << get_second_href_size() << std::endl;
		Sleep(3000);
	}


	return 0;
}


