#ifndef _H_IHTTP_H
#define _H_IHTTP_H
#include "../IHttp/HttpConnect.h"
#include "../IHttp/HttpSocket.h"
#include "../IHttp/IHttpInterface.h"

IHttpInterface* CreateHttpInstance();
void FreeInstance(IHttpInterface *pInstance);
bool ParseURL( const wstring& strUrl, wstring& strHome, wstring& strPage);

bool UrlDownload( const wstring& strUrl, const wstring& strSavePath, OUT UINT& uLoadSize, \
	OUT wstring& strErrorMsg, HWND hWnd/*=NULL*/, UINT uMsg/*=0*/ );
#endif