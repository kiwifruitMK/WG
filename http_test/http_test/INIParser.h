#ifndef INI_PARSER_H
#define INI_PARSER_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <map>

class ININode
{
public:
    ININode(std::string root, std::string key, std::string value)
    {
        this->root = root;
        this->key = key;
        this->value = value;
    }
    std::string root;
    std::string key;
    std::string value;
};

class SubNode
{
public:
    void InsertElement(std::string key, std::string value)
    {
        sub_node.insert(std::pair<std::string, std::string>(key, value));
    }
    std::map<std::string, std::string> sub_node;
};

class INIParser
{
public:
    int ReadINI(std::string path);
    std::string GetValue(std::string root, std::string key);
    std::vector<ININode>::size_type GetSize(){return map_ini.size();}
    std::vector<ININode>::size_type SetValue(std::string root, std::string key, std::string value);
    int WriteINI(std::string path);
    void Clear(){map_ini.clear();}
private:
    std::map<std::string, SubNode> map_ini;
};

#endif // INI_PARSER_H
